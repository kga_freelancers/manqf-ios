//
//  SideMenu.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class SideMenu: NSObject {
    var elementText = ""
    var elementIcon = UIImage()
    
    
    init(elementText: String, elementIcon : UIImage) {
        super.init()
        self.elementText = elementText
        self.elementIcon = elementIcon
    }
    
    
}
