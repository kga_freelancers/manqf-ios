//
//  CachingModel.swift
//  ESMA
//
//  Created by Nora on 10/14/18.
//  Copyright © 2018 BlueCrunch. All rights reserved.
//

import Foundation

class CachingModel{
    
    public struct CachingKeys  {
        static var  HOME_DATA_CACHE_KEY = "HomeDataCacheKey"
        static var  BRANDS_CACHE_KEY = "BrandsCacheKey"
         static var  BRANCH_CACHE_KEY = "BranchCacheKey"
        static var  SERVICE_CENTER_BRANDS_CACHE_KEY = "ServiceCenterBrandsCacheKey"
         static var  WARRANTY_BRANDS_CACHE_KEY = "WarrantyBrandsCacheKey"
        static var  USED_BRANDS_CACHE_KEY = "UsedBrandsCacheKey"
        static var  MODELS_CACHE_KEY = "ModelsCacheKey"
        static var  CATEGORIES_CACHE_KEY = "CategoriesCacheKey"
         static var  USED_CATEGORIES_CACHE_KEY = "UsedCategoriesCacheKey"
        static var  POPULAR_CATEGORIES_CACHE_KEY = "PopularCategoriesCacheKey"
        static var LATEST_OFFERS_CACHE_KEY = "LatestOffersCacheKey"
        static var NEWS_CACHE_KEY = "NewsCacheKey"
        static var SEARCH_DATA_CACHE_KEY = "SearchDataCacheKey"
        static var SELL_CAR_CACHE_KEY = "SellCarCacheKey"
        static var VALUE_CAR_CACHE_KEY = "ValueCarCacheKey"
        static var NOTIFICATION_STATUS = "notification"
        static var FONT_STATUS = "font"
        static var FIRST_TIME = "first_time"
    }
    
    
    
    static func getCachedData<T : Codable>(CACHE_KEY : String) -> (T)?
    {
        if let value = UserDefaults.standard.value(forKey: CACHE_KEY) as? Data{
            if let data = try? JSONDecoder().decode(T.self , from: value){
                return data
            }
        }
        return nil
    }
    
    static func setCached<T : Codable>(item : T , key : String){
        if let value = try? JSONEncoder().encode(item) {
            UserDefaults.standard.set(value, forKey: key)
        } else {
            UserDefaults.standard.set(nil, forKey: key)
        }
    }
}
