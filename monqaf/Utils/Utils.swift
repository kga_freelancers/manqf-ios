//
//  Utils.swift
//  Loreal Medical
//
//  Created by Nora on 3/18/18.
//  Copyright © 2018 Nora. All rights reserved.
//

import Foundation
import UIKit
import EventKit

enum AppStoryboard : String {
    case Main = "Main"
    case Home = "Home"
    case SideMenu = "SideMenu"

    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type)->T{
        let storyBoardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyBoardID ) as! T
    }
}

class Utils {
   
//    static func openHomeScreen(){
//       let vc = HomeViewController.instantiateFromStoryBoard(appStoryBoard: .Home)
//       let nvc = UINavigationController(rootViewController: vc)
//       nvc.navigationBar.isHidden = true
//       UIApplication.shared.windows[0].rootViewController = vc
//       UIApplication.shared.windows[0].makeKeyAndVisible()
//    }
//
//    static func openWalkthroughScreen(){
//        let vc = WalkthroughViewController.instantiateFromStoryBoard(appStoryBoard: .Main)
//        let nvc = UINavigationController(rootViewController: vc)
//        nvc.navigationBar.isHidden = true
//        UIApplication.shared.windows[0].rootViewController = nvc
//        UIApplication.shared.windows[0].makeKeyAndVisible()
//    }
//  
////    static func openLoginScreen(){
////        let nvc = UINavigationController(rootViewController: LoginRouter.createModule())
////        nvc.navigationBar.isHidden = true
//////        UIApplication.shared.windows[0].rootViewController = nvc
//////        UIApplication.shared.windows[0].makeKeyAndVisible()
////    }
//
//    static func openLandingScreen(){
//       let vc = LandingViewController.instantiateFromStoryBoard(appStoryBoard: .Main)
////        let vc = NearByEventsRouter.createModule()
//        let nvc = UINavigationController(rootViewController: vc)
//        nvc.navigationBar.isHidden = true
//        UIApplication.shared.windows[0].rootViewController = nvc
//        UIApplication.shared.windows[0].makeKeyAndVisible()
//    }
//
//    static func openSplashScreen() {
//        let vc = SplashViewController.instantiateFromStoryBoard(appStoryBoard: .Main)
//        UIApplication.shared.windows[0].rootViewController = vc
//        UIApplication.shared.windows[0].makeKeyAndVisible()
//    }
//
//    static func getNavigationController()->UINavigationController?{
//        let root = (UIApplication.shared.windows[0].rootViewController)
//        if root is UINavigationController{
//            return root as? UINavigationController
//        }else{
//            return nil
//        }
//    }
//
//    static func getNotificationStatus()->Bool{
//        if let status = UserDefaults.standard.value(forKey: Constants.NOTIFICATIONS) as? Bool{
//            return status
//        }
//        return true
//    }
//
    static func navigateToMap(latitude : String , longitude : String){

        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            if latitude != "" && longitude != "" {
            let mURL = "comgooglemaps://?daddr=\(latitude),\(longitude)"
            UIApplication.shared.open(URL(string: mURL)!, options: [:], completionHandler: nil)
         }
        } else {
            print("Can't use comgooglemaps://")

            if latitude != "" && longitude != "" {
                let mURL = "http://maps.google.com/?daddr=\(latitude),\(longitude)"
                UIApplication.shared.open(URL.init(string: mURL)!, options: [:], completionHandler: nil)
            }
        }
    }
//
//
//    static func getLanguage() -> String
//    {
//        if let language = UserDefaults.standard.value(forKey: "current_language") as? String{
//            return language
//        }
//        return "en"
//    }
//
//    static func isUserLoggedIn() -> Bool {
//        let user = UserDataSource.loadUser()
//        let token = user?.token ?? ""
//
//        if token != "" {
//
//            return true
//        }else{
//            return false
//        }
//    }
//
//    static func appIsArabic()->Bool{
//        return getLang() == "ar"
//    }
//
//    static func getLang()->String{
//        if let lang = UserDefaults.standard.value(forKey: "lang") as? String{
//            return lang
//        }else{
//            print(Locale.preferredLanguages[0])
//            let lang = Locale.preferredLanguages[0].contains("ar") ? "ar" : "en"
//            UserDefaults.standard.set(lang, forKey: "lang")
//            return lang
//        }
//    }
//
//    static func isNotificationOn() -> Bool {
//        let notificationStatus = UserDefaults.standard.value(forKeyPath: CachingModel.CachingKeys.NOTIFICATION_STATUS) as? Bool ?? true
//
//        return notificationStatus
//    }
//
//
//    static func setNotificationStatus(){
//
//        let status = isNotificationOn()
//        let notifcationStatus = status ? false : true
//        UserDefaults.standard.set(notifcationStatus, forKey: CachingModel.CachingKeys.NOTIFICATION_STATUS)
//
//        if status {
//        UIApplication.shared.registerForRemoteNotifications()
//        }else{
//        UIApplication.shared.unregisterForRemoteNotifications()
//        }
//   }
//
//    static func setFontSizeStatus(status : Int){
//        if status == 0 {
//            Constants.fontScale = 0.9
//
//        }else if status == 1 {
//            Constants.fontScale = 1.0
//        }else if status == 2 {
//            Constants.fontScale = 1.1
//        }
//       UserDefaults.standard.set(status, forKey: CachingModel.CachingKeys.FONT_STATUS)
//    }
//
//
//    static func getFontSizeStatus() -> Int {
//        let status =  UserDefaults.standard.string(forKey: CachingModel.CachingKeys.FONT_STATUS) ?? "1"
//        return Int(status) ?? 1
//    }
//
//    static func isFirstTime() -> Bool {
//
//      let status =  UserDefaults.standard.string(forKey: CachingModel.CachingKeys.FIRST_TIME) ?? "true"
//      let currentStatus = Bool(status) ?? true
//
//        UserDefaults.standard.set("false", forKey: CachingModel.CachingKeys.FIRST_TIME)
//
//        return currentStatus
//    }
//
//   static func addMatchToCalendar(match : Match) {
//         let currentVc = UIApplication.shared.windows[0].visibleViewController
//        let eventStore = EKEventStore()
//
//        switch EKEventStore.authorizationStatus(for: .event) {
//        case .authorized:
//            insertEvent(store: eventStore, match : match)
//        case .denied:
//           currentVc?.showMessage("Access denied".localize())
//        case .notDetermined:
//            // 3
//            eventStore.requestAccess(to: .event, completion:
//                { (granted: Bool, error: Error?) -> Void in
//                    if granted {
//                        self.insertEvent(store: eventStore,match: match)
//                    } else {
//                       currentVc?.showMessage("Access denied".localize())
//                    }
//            })
//        default:
//            print("Case default")
//        }
//    }
//
//  static func insertEvent(store: EKEventStore,match : Match) {
//         let currentVc = UIApplication.shared.windows[0].visibleViewController
//        let event = EKEvent(eventStore: store)
//        event.title = match.competition?.name
//        event.startDate = match.date?.toDate()
//        event.endDate = match.date?.toDate()
//        event.notes = match.competition?.description ?? "" + "  \(String(describing: match.homeTeam?.title))" + "  \(String(describing: match.awayTeam?.title))"
//
//        event.calendar = store.defaultCalendarForNewEvents
//
//        do {
//            try store.save(event, span: .thisEvent)
//             currentVc?.showMessage("Event added successfully to calendar".localize())
//        }
//        catch {
//             currentVc?.showMessage("Error saving event in calendar".localize())
//        }
//    }
//
//    static func downloadBrochure(fileUrl : String){
//
//
////        let vc = BasicWebViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
////        vc.fileUrl = fileUrl
////    UIApplication.shared.windows[0].visibleViewController?.present(vc, animated: true, completion: nil)
////
////
//    }
//
//
    static func call (number : String){
        guard let number = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(number)
    }

    static func share(vc : UIViewController,link : String) {

        let textToShare = [link]//[URL.init(string: link)]
        let activityViewController = UIActivityViewController(activityItems: textToShare as [Any],
                applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.airDrop]

        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2,
                    y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
            popoverController.sourceView = vc.view
            popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }
        vc.present(activityViewController, animated: true, completion: nil)

    }
//
    static func openWebsite(link : String) {
        guard let url = URL(string: link) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
//
//    static func openMail(email : String) {
//        if let url = URL(string: "mailto:\(email)") {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url)
//            } else {
//                UIApplication.shared.openURL(url)
//            }
//        }
//    }
//
//    static func validateYouTubeLink(youtubeURL : String) -> Bool {
//        let regex = "^((?:https?:)?\\/\\/)?((?:www|m)\\.)?((?:youtube\\.com|youtu.be))(\\/(?:[\\w\\-]+\\?v=|embed\\/|v\\/)?)([\\w\\-]+)(\\S+)?$"
//
//        let test = NSPredicate(format:"SELF MATCHES %@", regex)
//        return test.evaluate(with: youtubeURL)
//    }
//
//    static func getYoutubeImageLink(youtubeUrl: String) -> String? {
//        if(youtubeUrl.youtubeID != nil){
//            return "http://img.youtube.com/vi/"+youtubeUrl.youtubeID!+"/mqdefault.jpg"
//        }
//        else{
//            return ""
//        }
//
//    }
//    static func getYoutubeVideoId(youtubeUrl: String) -> String? {
//        return youtubeUrl.youtubeID!
//    }
//
//

    

}
