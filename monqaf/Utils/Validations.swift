//
//  Validations.swift
//  MyLexus
//
//  Created by Nora on 6/19/18.
//  Copyright © 2018 Nora. All rights reserved.
//

import Foundation

class Validations {

    static func isValidPhoneNumber (text : String) -> Bool{
        if  text.count >= 6 && text.isNumeric(){
          return true
        }else{
            return false
        }
    }
    
    static func doStringContainsNumber(text : String) -> Bool{
        let numberRegEx  = ".*[0-9]+.*"
        let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let containsNumber = testCase.evaluate(with: text)
        
        return containsNumber
    }
    
    static func isValidEmail(text : String) -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        return test.evaluate(with: text)
    }
    
    static func textHasOnlyNumbers(_ text: String) -> Bool {
        
        let charcterSet  = CharacterSet(charactersIn: "0123456789").inverted
        let inputString = text.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  text == filtered
    }
    
   static func hasSpecialCharOnly(_ text : String) -> Bool {
     let specialChar = CharacterSet(charactersIn:
        "\\-|!#$%&/()=?»«@£§€{}.-;'<>_,").inverted

     let inputString  = text.components(separatedBy: specialChar)
     let filtered = inputString.joined(separator: "")
    return text == filtered
    }

    static func hasSpecialChar(_ text : String) -> Bool {
        let specialChar = CharacterSet(charactersIn:
            "\\-|!#$%&/()=?»«@£§€{}.-;'<>_,").inverted

        let inputString  = text.components(separatedBy: specialChar)
        let filtered = inputString.joined(separator: "")
        return filtered.count > 0
    }

    static func hasSpecialCharAndNumbersOnly(_ text : String) -> Bool {

        let specialChar = CharacterSet(charactersIn:
            "\\-|!#$%&/()=?»«@£§€{}.-;'<>_,0123456789").inverted

        let inputString  = text.components(separatedBy: specialChar)
        let filtered = inputString.joined(separator: "")
        return text == filtered

    }



    static func isValidPassword(text: String)->Bool{
       let regex = "^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#@?!@$%^&_*-]).{8,}$"
        let userTest = NSPredicate(format: "SELF MATCHES %@", regex)
          return userTest.evaluate(with: text)
    }
    
    static func isValidName(string : String)->Bool{
        
        if textHasOnlyNumbers(string){
            return false
        }

        if hasSpecialCharOnly(string){
            return false
        }

        if hasSpecialCharAndNumbersOnly(string){
        return false
        }

        let regex =  "^([0-9a-zA-Zء-ي ]).{3,30}$"
        let userTest = NSPredicate(format: "SELF MATCHES %@", regex)
        return userTest.evaluate(with: string) || hasSpecialChar(string)
    }




}
