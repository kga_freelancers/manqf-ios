//
//  Constants.swift
//  Loreal Medical
//
//  Created by Nora on 3/18/18.
//  Copyright © 2018 Nora. All rights reserved.
//

import Foundation
import UIKit

//c19cd423-70de-4740-93c4-ed8b6f459376
//MTk5MWQ5ZDgtOWM2Yy00ZDVkLWI5NTAtZmNhNmMwNmRmNGYz

class Constants{
    
    
    struct ONE_SIGNAL {
         static var APP_ID = "c19cd423-70de-4740-93c4-ed8b6f459376"
     }
    struct Events {
        static var OPEN_BRANDS_TAB = "OPEN_BRANDS_TAB"
   
    }
    
    struct TournmentType {
        static var EMIRATES = "emirates"
        static var NORMAL = "normal"
    }
    
    struct NotificationTopics{
        static var general = "all_ios"
    }
    
    
    static var PER_PAGE = 1
    static var NOTIFICATIONS = "NOtifications"
    static var UNREAD_NOTIFICATION = "UNREAD_NOTIFICATION"
    static var WEBSITE_URL = "https://www.adsc.com/"
    static var VOULANTEER_LINK = "https://www.volunteers.ae/index.aspx"
   static var APP_Link =  "https://apps.apple.com/us/app/المنقف/id1489868718?ign-mpt=uo%3D2"
    static var EVENT = "Event"
    static var TASK = "Task"
    static var APPT_THEME = "App Theme"
    static var CHECKIN_DISTANCE =  2.0
 
    static var GOOGLE_API_KEY = "AIzaSyDnJekMygQEafLWOa3Gs9VeH8sVHn_jaag"
    
    static var fontScale : CGFloat = 0.9
    // shared prefeences
    
    
    //notification center
    static var MY_POINTS = "Points"
    static var WEEKLY_CHALLENGE = "Challenge"
    struct User {
        static var USER = "user"
    }

    struct SocialFeedTypes {
        static var YOUTUBE = "Youtube"
        static var TWITTER = "Twitter"
        static var INSTAGRAM = "Instagram"
        static var FACEBOOK = "Facebook"
    }
    
    struct AppTheme {
        static var LIGHT = "light"
        static var DARK = "dark"
    }
    
}
