//
//  inputField.swift
//  Shezlong
//
//  Created by George Naiem on 8/6/17.
//  Copyright © 2017 Bluecrunch. All rights reserved.
//

import UIKit

protocol InputFeildDelegate {
    func shouldBeginEditing(inputFeild : InputField)
    func didEndEditing(inputFeild : InputField)
    func didBeginEditing(inputFeild : InputField)
    func shouldReturn(inputFeild : InputField)
    func edittingChanged(inputFeild : InputField)
}

class InputField: BaseNibLoader  {
    
    public enum feildType : String{
        case email = "email"
        case password = "password"
        case phoneNumber = "phoneNumber"
        case regular = "regular"
        case name = "name"
        case action = "action"
        case number = "number"
   }

    
    
    @IBOutlet weak var flagSperatorView: UIView!
    @IBOutlet weak var numberViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var flagImageView : UIImageView!
    @IBOutlet weak var flagArrow : UIImageView!
    @IBOutlet weak var flagBtn: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var actionBtn : UIButton!
    @IBOutlet weak var lineView : UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var downArrow: UIImageView!
    @IBOutlet weak var eyeView: UIView!
    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var eyeWidthConstraint: NSLayoutConstraint!
 
    @IBAction func eyeBtnPressed(_ sender: Any) {
       
            field.isSecureTextEntry = !field.isSecureTextEntry
            
            if !field.isSecureTextEntry{
                eyeBtn.setImage(UIImage(named: "openEye"), for: .normal)
            }else{
                eyeBtn.setImage(UIImage(named: "closeEye"), for: .normal)
            }
        }
        
    
    var actionHandler: ((Any)->Void)?
    var type : feildType = .regular {
        didSet{
            if actionBtn != nil
            {
                actionBtn.isHidden = self.type != .action
                
            }
        }
    }
    
    var delegate : InputFeildDelegate!
    var optional = false
    
    @IBInspectable  var feildTitle : String = ""{
        didSet{
            titleLbl.text = feildTitle
        }
    }
    @IBInspectable var errorMsg = ""{
        didSet{
            
            errorLabel.text = errorMsg
        }
    }
    @IBInspectable  var feildPlaceHolder : String = ""{
        didSet{
            field.placeholder = feildPlaceHolder
        }
    }
    
    func setLineColor(color : UIColor){
     lineView.borderColor = color
    }
    
    @IBAction func buttonPressed(_ sender : Any){
        if actionHandler != nil{
            actionHandler!(sender)
        }
    }

    
   var textColor = Colors.textFeildFontColor

  
    func reSet(){
        setText(text: "")
        field.leftView = UIView()
        normal()
    }

    override func awakeFromNib() {
        numberView.semanticContentAttribute = .forceLeftToRight
        
        if appIsArabic(){
            field.textAlignment = .right
        }
       textColor = field.textColor!
        field.delegate = self
        self.field.addTarget(self, action: #selector(textFeildChanged), for: .editingChanged)
        self.normal()
    }
    
    func invalid(error: String) {
        field.isEnabled = true
        errorLabel.text = error
        errorLabel.isHidden = false
        errorLabel.textColor = UIColor.init(codeString: "#FF0000")
        setLineColor(color: UIColor.init(codeString: "#FF0000"))
 }
 
    func dim() {
        field.isEnabled = false
    }
    
    func normal() {
        field.isEnabled = true
        field.textColor = textColor
        setLineColor(color: UIColor.init(codeString: "#bd945b"))
        errorLabel.isHidden = true
        errorLabel.text = ""
    }
    
    func getText() -> String {
        return field.text!
    }
    func setText(text: String){
        field.text = text
    }
    
    func isEmptyAndNotOptional()->Bool{
        return self.getText().trim().isEmpty && !optional
    }
    
    
    func validate()->Bool{
        var isValid = true
        switch type {
        case .action:
            if isEmptyAndNotOptional(){
                isValid = false
                let errorMsg = " \("please select ".localize())\(titleLbl.text ?? "")"
                self.invalid(error: errorMsg)
            }
            break
        case .number:
            if isEmptyAndNotOptional(){
                isValid = false
                self.invalid(error: (errorMsg != "") ?
                    errorMsg : "please enter your password".localize())
            }
        case .email:
            if isEmptyAndNotOptional(){
                isValid = false
                self.invalid(error: "please enter your email".localize())
            }else if !optional || (optional && getText().trim() != ""){
                    if !Validations.isValidEmail(text: self.getText().trim()){
                        isValid = false
                        self.invalid(error: "email is not valid".localize())
                    }
            }
            break
        case .password:
            if  isEmptyAndNotOptional(){
                isValid = false
                self.invalid(error: "please enter your password".localize())
            }else{
                if !optional || (optional && getText().trim() != ""){
                    if !Validations.isValidPassword(text: self.getText()) {
                        isValid = false
                        self.invalid(error: "Password must be 8 characters long, with at least 1 number and special character".localize())
                    }
                }
            }
            break
        case .phoneNumber:
            if  isEmptyAndNotOptional(){
                isValid = false
                self.invalid(error: "please enter your phone number".localize())
            }else{
                if !optional || (optional && getText().trim() != ""){
                    if !Validations.isValidPhoneNumber(text: self.getText().trim()) {
                        isValid = false
                        self.invalid(error: "phone number is not valid".localize())
                    }
                }
            }
            break
      
        case .name:
            if isEmptyAndNotOptional(){
                isValid = false
                self.invalid(error: (errorMsg != "") ?
                    errorMsg : "please enter your name".localize())
            }else{
                if !optional || (optional && getText().trim() != ""){
                    if !Validations.isValidName(string: self.getText()) {
                        isValid = false
                        self.invalid(error: "name is not valid".localize())
                    }
                }
           }
            break
        case .regular:
            if isEmptyAndNotOptional(){
                isValid = false
                self.invalid(error: (errorMsg != "") ?
                    errorMsg :
                    "field can't be empty".localize())
            }
            break

        }
        
        if isValid {
            invalid(error: "")
            setLineColor(color: UIColor.init(codeString: "#bd945b"))
            }else{
            setLineColor(color: UIColor.init(codeString: "#FF0000"))
            }
        return isValid
    }
}

extension InputField : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if delegate != nil{
            delegate.didBeginEditing(inputFeild: self)
        }
        self.normal()
       // setLineColor(color: Colors.appPrimeryColor)
    }
    
    @objc func textFeildChanged(_ sender : UITextField){
        if delegate != nil{
            delegate.edittingChanged(inputFeild: self)
        }
        if type != .password{
            setText(text: getText().updateToEngNum())
        }
//        setLineColor(color: Colors.appPrimeryColor)
        
       
//        let status = validate()
//
//        if status {
//            invalid(error: "")
//            setLineColor(color: UIColor.init(codeString: "#D5D8D9"))
//        }else{
//            setLineColor(color: UIColor.init(codeString: "#FF0000"))
//        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if delegate != nil{
            delegate.shouldBeginEditing(inputFeild: self)
        }
        self.normal()
//        setLineColor(color: Colors.appPrimeryColor)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if delegate != nil{
            delegate.shouldReturn(inputFeild: self)
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if delegate != nil{
            delegate.didEndEditing(inputFeild: self)
        }
     //   setLineColor(color: Colors.textFeildLineColor)
      //  let _ = validate()
    }
}
