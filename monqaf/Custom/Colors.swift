//
//  Colors.swift
//  Shezlong
//
//  Created by George Naiem on 8/6/17.
//  Copyright © 2017 Bluecrunch. All rights reserved.
//

import UIKit

class Colors: Any {
    static var red = UIColor.init(codeString: "#FF0000")
    static var appRed = UIColor.init(codeString: "#E50615")
    static var textFeildTitleColor = UIColor.init(codeString: "#2E313C")
    static var textFeildFontColor = UIColor.init(codeString: "#43425D")
    static var textFeildHolderColor = UIColor.init(codeString: "#C7C7CD")
    static var textFeildLineColor = UIColor.init(codeString: "#D8D8D8")
    static var textViewFontColor = UIColor.init(codeString: "#2E313C")
    static var textViewHolderColor = UIColor.init(codeString: "#C7C7CD")
    static var appPrimeryColor = UIColor.init(codeString: "#1474E6")
    static var textDarkBlue = UIColor.init(codeString: "#232938")
    
    static var timeSlotUnselected = UIColor.init(codeString: "#f9f9f9")

    //Booking status colors
    static var pending = UIColor.init(codeString: "#f5b136")
    static var confirmed = UIColor.init(codeString: "#821eac")
    static var canceled = UIColor.init(codeString: "#d0021b")
    static var underExmination = UIColor.init(codeString: "#1e31ac")
    static var exminationResult = UIColor.init(codeString: "#1ea5ac")
    static var inprogress = UIColor.init(codeString: "#000000")
    static var done = UIColor.init(codeString: "#1eac24")
    static var reserved = UIColor.init(codeString: "#7FA9C5")

    static var pagerSelectedBarColor = UIColor.init(codeString: "#f5b136")
    static var tabBarNotSelected = UIColor.init(codeString: "#8e8e93")
    
}

