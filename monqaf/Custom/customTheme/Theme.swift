//
//  Theme.swift
//  ADSC
//
//  Created by Karim abdelhameed mohamed on 9/16/19.
//  Copyright © 2019 SherifShokry. All rights reserved.
//

import UIKit

struct Theme {
    
    static var backgroundColor:UIColor?
    static var buttonTextColor:UIColor?
    static var buttonBackgroundColor:UIColor?
    static var accentButtonBackgroundColor:UIColor?
    static var ImageBackgroundColor:UIColor?
    static var labelTextColor:UIColor?
    static var labelBgColor:UIColor?
    static var cardBgColor:UIColor?
    static var clearBgViewColor:UIColor?
    static var whiteBgViewColor:UIColor?
    static var blackLabelColor:UIColor?
    static var subTitleLabelColor:UIColor?
    static var textFieldColor:UIColor?
    
    static var keyboardAppearence : UIKeyboardAppearance?

    
    static public func darkTheme() {
        self.backgroundColor = UIColor.init(codeString: "#555555")
        self.buttonTextColor = UIColor.white
        self.buttonBackgroundColor = UIColor.clear
        self.ImageBackgroundColor = UIColor.clear
        self.labelTextColor = UIColor.white
        self.labelBgColor = UIColor.clear
        self.accentButtonBackgroundColor = UIColor.init(codeString: "#bd945b")
        self.cardBgColor = UIColor.init(codeString: "#3e3e3e")
        self.clearBgViewColor = UIColor.clear
        self.whiteBgViewColor = UIColor.white
        self.blackLabelColor = UIColor.black
        self.subTitleLabelColor = UIColor.init(codeString: "#8E8E8E", alpha: 0.87)
        self.textFieldColor = UIColor.white
        self.keyboardAppearence = .dark
    }
    
    

}
