//
//  News.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import ObjectMapper


class News : Mappable {
    
    var id : Int?
    var imageURL : String?
    var images : [String]?
    var title : String?
    var date : String?
    var newsDescription : String?
    var description : String?
    var newsType : Int?
    
    
    init() {
    }
    
    required init?(map: Map) {
        id = -1
        imageURL = ""
        newsType = -1
        title = ""
        date = ""
        newsDescription = ""
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        imageURL <- map["imageUrl"]
        newsType <- map["newsType"]
        title <- map["title"]
        images <- map["images"]
        newsDescription <- map["desc"]
        description <- map["description"]
        date <- map["date"]
    }

}

class NewsResponse: BaseResponse {
    var newsList : NewsList?
    
    override init() {
        super.init()
    }
    required init?(map: Map) {
        super.init(map: map)
    }
     override func mapping(map: Map) {
        super.mapping(map: map)
        newsList <- map["result"]
    }
    
    static func getNewsResponse(dict : [String:Any]) ->  NewsResponse {
        return Mapper<NewsResponse>().map(JSON: dict)!
    }
}

class NewsList: Mappable {
    
    var list = [News]()
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        list <- map["newsDtos"]
    }
    
    init() {
    }

}

