//
//  HomePagerCell.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import FSPagerView
import Cloudinary

class HomePagerCell: FSPagerViewCell {
    
    @IBOutlet weak var sliderImageView : UIImageView!
    @IBOutlet weak var titleLablView : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(slider : Slider) {
        
        
        sliderImageView.sd_setImage(with: URL.init(string: slider.imageUrl?.getCloudNariImageUrl() ?? ""), completed: nil)
        titleLablView.text = slider.title
    }

}
