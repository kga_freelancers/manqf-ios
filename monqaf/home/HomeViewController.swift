//
//  HomeViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import FSPagerView
import KVNProgress
import BarcodeScanner

class HomeViewController: UIViewController {
    
    var presenter : ViewToPresenterHomeProtocol?
    var sliderData : SliderResponse?
    
    @IBOutlet weak var pageControl : FSPageControl!
    
    @IBOutlet weak var pagerController: FSPagerView!{
        didSet {
            self.pagerController.register(UINib(nibName: "HomePagerCell", bundle: nil), forCellWithReuseIdentifier: "HomePagerCell")
            self.pagerController.interitemSpacing = 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pagerController.delegate = self
        pagerController.dataSource = self
        
        pageControl.setFillColor(UIColor.init(codeString: "#FFC138"), for: .selected)
        pageControl.setFillColor(UIColor.init(codeString: "#00018E"), for: .normal)
        
        KVNProgress.show()
        presenter?.loadSlider()
    }
    
    @IBAction func boardMembersAction( _sender : Any){
        let vc = BoardMemberRouter.createModule()
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func newsAction( _sender : Any){
        self.present(NewsRouter.createModule(title: SideMenuKeys.NEWS.rawValue,fromWhere: .NEWS), animated: true, completion: nil)
    }
    @IBAction func socialNewsAction( _sender : Any){
        self.present(NewsRouter.createModule(title: SideMenuKeys.SOCIAL_NEWS.rawValue,fromWhere: .SOCIAL_NEWS), animated: true, completion: nil)
    }
    
    @IBAction func offersAction(_ sender:Any){
        self.present(NewsRouter.createModule(title: SideMenuKeys.OFFERS.rawValue,fromWhere: .OFFERS), animated: true, completion: nil)
    }
    
    @IBAction func contactUsAction(_ sender:Any){
        let vc = ContactUsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func reportAproductAction(_ sender:Any){
        let vc = ReportProductViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func galleryAction(_ sender:Any){
        self.present(NewsRouter.createModule(title: SideMenuKeys.GALLERY.rawValue,fromWhere: .MEDIA), animated: true, completion: nil)
    }
    
    @IBAction func proofAction(_ sender:Any){
        self.present(ProofRouter.createModule(), animated: true, completion: nil)
    }
    
    @IBAction func searchAction(_ sender:Any){
        self.present(SearchRouter.createModule(), animated: true, completion: nil)
    }
    
    @IBAction func showAllOffersAction(_ sender:Any){
        self.present(NewsRouter.createModule(title: SideMenuKeys.OFFERS.rawValue,fromWhere: .OFFERS), animated: true, completion: nil)
    }
    
    @IBAction func barcodeAction(_ sender:Any){
        let viewController = BarcodeScannerViewController()
        viewController.isOneTimeSearch = true
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        
        present(viewController, animated: true, completion: nil)
    }
}

extension HomeViewController : BarcodeScannerCodeDelegate{
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print(code)
        controller.dismiss(animated: true) {
            self.present(BarcodeResultRouter.createModule(barcode: code), animated: true, completion: nil)
        }
    }
    
    
}
extension HomeViewController : BarcodeScannerErrorDelegate{
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        //        controller.resetWithError(message: "لا يوجد منتج بهذه التفاصيل .. حاول مجددا")
        print(error.localizedDescription)
    }
    
    
}
extension HomeViewController : BarcodeScannerDismissalDelegate{
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func profitsAction(_ sender: Any) {
        let vc = ProfitsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func branchesAction(_ sender: Any) {
        let vc = BranchesViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func QuestionsAnswersAction(_ sender: Any) {
        let vc = QuestionsAnswersViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadOfferById(id : Int) {
        KVNProgress.show()
        let dataSource = GeneralDataSource()
        dataSource.loadOfferById(id : id){ (status, response) in
            KVNProgress.dismiss()
            switch status {
            case .sucess :
                let offer = (response as? Offer) ?? Offer()
                self.openOfferDetails(offer: offer)
                break
            case .error :
                self.showMessage(response as? String ?? "")
                break
            case .networkError:
                self.showMessage(response as? String ?? "")
                break
            }
        }
    }
    
    func loadNewsById(id : Int) {
        let dataSource = GeneralDataSource()
        KVNProgress.show()
        dataSource.loadNewsById(id : id){ (status, response) in
            KVNProgress.dismiss()
            switch status {
            case .sucess :
                let news = (response as? News) ?? News()
                self.openNewsDetails(news: news)
                break
            case .error :
                self.showMessage(response as? String ?? "")
                break
            case .networkError:
                self.showMessage(response as? String ?? "")
                break
            }
        }
    }
    
    func openOfferDetails(offer : Offer)  {
        let vc = OfferDetailsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        vc.offer = offer
        self.present(vc, animated: true, completion: nil)
    }

    func openNewsDetails(news : News)  {
        let vc = OfferDetailsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        vc.news = news
        self.present(vc, animated: true, completion: nil)
    }
    
}



extension HomeViewController : PresenterToViewHomeProtocol {
    func showHomeData(data: SliderResponse) {
        self.sliderData = data
        pagerController.reloadData()
        KVNProgress.dismiss()
    }
    
    func showError(error: String) {
        self.showMessage(error)
        KVNProgress.dismiss()
    }
}

extension HomeViewController : FSPagerViewDataSource
{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        pageControl.numberOfPages = sliderData?.sliderData?.sliderList?.count ?? 0
        return sliderData?.sliderData?.sliderList?.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "HomePagerCell", at: index) as!  HomePagerCell
        cell.setData(slider: sliderData?.sliderData?.sliderList?[index] ?? Slider())
        pageControl.currentPage = index
        
        return cell
    }
    
    @objc func SwipeLeftImage(){
        print("Swipe Left")
    }
    @objc func SwipeRightImage(){
        print("Swipe Right")
        
    }
}

extension HomeViewController : FSPagerViewDelegate
{
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
        if self.sliderData?.sliderData?.sliderList?[index].sliderType == 1{
            self.loadNewsById(id: self.sliderData?.sliderData?.sliderList?[index].id ?? 0)
        }else if  self.sliderData?.sliderData?.sliderList?[index].sliderType == 2 {
             self.loadOfferById(id: self.sliderData?.sliderData?.sliderList?[index].id ?? 0)
        }
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        // update arrows and title
        pageControl.currentPage = targetIndex
        
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        pageControl.currentPage = index
    }
}
