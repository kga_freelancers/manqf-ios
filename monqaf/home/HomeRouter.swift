//
//  HomeRouter.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

class HomeRouter: PresenterToRouterHomeProtocol {
    
    static func createModule() -> UIViewController {
        
                let view = HomeViewController.instantiateFromStoryBoard(appStoryBoard: .Main)
                let presenter : ViewToPresenterHomeProtocol & InteractorToPresenterHomeProtocol = HomePresenter()
                
                let interactor : PresenterToIntetractorHomeProtocol = HomeInteractor()
                let router : PresenterToRouterHomeProtocol = HomeRouter()
                
                view.presenter = presenter
                presenter.interactor = interactor
                presenter.view = view
                presenter.router = router
                interactor.presenter = presenter
                return view
            }
    }

