//
//  HomeProtocol.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterHomeProtocol: class {
    
    var view : PresenterToViewHomeProtocol? {get set}
    var interactor : PresenterToIntetractorHomeProtocol? {get set}
    var router : PresenterToRouterHomeProtocol? {get set}
    func loadSlider()
}

protocol PresenterToViewHomeProtocol: class {
    func showHomeData(data : SliderResponse)
    func showError(error : String)    
}

protocol PresenterToIntetractorHomeProtocol: class {
    var presenter: InteractorToPresenterHomeProtocol? { get set }
    
    func loadData()
    
}

protocol PresenterToRouterHomeProtocol: class  {
    static func createModule() -> UIViewController
    
}

protocol InteractorToPresenterHomeProtocol: class {
    func showData(data : SliderResponse)
    func showError(error : String)
}

