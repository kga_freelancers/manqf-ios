//
//  HomeInteractor.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class HomeInteractor : PresenterToIntetractorHomeProtocol {
    var presenter: InteractorToPresenterHomeProtocol?
    
    
    func loadData() {
        let dataSource = GeneralDataSource()
        dataSource.loadHome{ (status, response) in
            switch status {
            case .sucess :
                self.presenter?.showData(data: response as? SliderResponse
                    ?? SliderResponse())
                break
            case .error :
                self.presenter?.showError(error: response as? String ?? "")
                break
            case .networkError:
                self.presenter?.showError(error: response as? String ?? "")
                break
            }
        }
    }
}
