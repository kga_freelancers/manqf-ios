//
//  HomePresenter.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class HomePresenter : ViewToPresenterHomeProtocol {
    
    
    
    var view: PresenterToViewHomeProtocol?
    var interactor: PresenterToIntetractorHomeProtocol?
    var router: PresenterToRouterHomeProtocol?
    
    func loadSlider() {
        interactor?.loadData()
    }
}
extension HomePresenter : InteractorToPresenterHomeProtocol{
   
    func showData(data: SliderResponse) {
        view?.showHomeData(data: data)
    }
    
    func showError(error: String) {
        view?.showError(error: error)
    }
}

