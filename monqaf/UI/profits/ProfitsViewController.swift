//
//  ProfitsViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import ContentSheet
import KVNProgress


class ProfitsViewController: UIViewController {

    @IBOutlet weak var nationalIdField: UITextField!
    @IBOutlet weak var errorNationalId: UILabel!
    @IBOutlet weak var boxNumber: UITextField!
    @IBOutlet weak var errorBoxNumber: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nationalIdField.delegate = self
        boxNumber.delegate = self
        addTargets()
    }
    
    func addTargets() {
        nationalIdField.addTarget(self, action:#selector(ProfitsViewController.textFieldDidChange(_:)),
                                  for: UIControl.Event.editingChanged)
        boxNumber.addTarget(self, action: #selector(ProfitsViewController.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
    }
    
    
    
    @IBAction func submitAction(_ sender: Any) {
        if validate() {
            loadProfits()
        }
    }
    
    func loadProfits() {
        KVNProgress.show()
        let dataSource = GeneralDataSource()
        dataSource.loadProfits(nationalId: nationalIdField.text ?? ""
        , boxNumber: boxNumber.text ?? ""){ (status, response) in
            KVNProgress.dismiss()
            switch status {
            case .sucess :
                self.openProfitDetails(profit: (response as? ProfitsResponse)?.profitResult.profit ?? Profit())
                break
            case .error :
                self.showMessage(response as! String)
                break
            case .networkError:
                self.showMessage(response as! String)
                break
            }
        }
     
    }
    
    func openProfitDetails(profit  : Profit)  {
        
        if profit.id == 0  {
            self.showMessage("من فضلك تاكد من صحة البيانات المدخلة")
            return
        }
        
        let content: ContentSheetContentProtocol
        
        let mVC = ProfitsDetailsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        mVC.profit = profit
        let contentController = mVC
        content = contentController
        let contentSheet = ContentSheet(content: content)
        contentSheet.blurBackground = false
        contentSheet.showDefaultHeader = false
        self.present(contentSheet, animated: true, completion: nil)
    }
    
    func validate() -> Bool {
        var isValid = true
        if nationalIdField.text?.isEmpty ?? false {
            isValid = false
            errorNationalId.isHidden = false
        }
        
        if boxNumber.text?.isEmpty ?? false {
            isValid = false
            errorBoxNumber.isHidden = false
        }
        return isValid
    }
    
}

extension ProfitsViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == nationalIdField && (nationalIdField.text?.count)! > 0 {
            errorNationalId.isHidden = true
        }
        
        if textField == boxNumber && (boxNumber.text?.count)! > 0 {
            errorBoxNumber.isHidden = true
        }
    }
    
}
