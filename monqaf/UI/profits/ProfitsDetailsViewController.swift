//
//  ProfitsDetailsViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import ContentSheet

class ProfitsDetailsViewController: UIViewController {

    @IBOutlet weak var boxNumberLabl: UITextField!
    @IBOutlet weak var contriputerLabl: UITextField!
    @IBOutlet weak var buyProfitLabl: UITextField!
    @IBOutlet weak var arrowsProfits: UITextField!
    @IBOutlet weak var totlaProfitsLabl: UITextField!
    
    var profit = Profit()
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()

    }
    
    override func expandedHeight(containedIn contentSheet: ContentSheet) -> CGFloat {
        return 1000
    }
    
    override func collapsedHeight(containedIn contentSheet: ContentSheet) -> CGFloat {
        return 800
    }
    
    func setData()  {
        boxNumberLabl.text = profit.boxNo
        contriputerLabl.text = profit.name
        buyProfitLabl.text = "\(profit.purchaseAmount ?? 0)"
        arrowsProfits.text = "\(profit.purchasePercentage ?? 0)"
        totlaProfitsLabl.text = "\(profit.totalProfit ?? 0)"
    }
    
    @IBAction func closeVc(_ sender : Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
