//
//  ContactUsViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import KVNProgress

class ContactUsViewController: UIViewController,UIActionSheetDelegate {

    @IBOutlet weak var nameField : UITextField!
    @IBOutlet weak var phoneField : UITextField!
    @IBOutlet weak var emailField : UITextField!
    @IBOutlet weak var reaseonField : UITextField!
    @IBOutlet weak var messageField : UITextView!
    @IBOutlet weak var errorName : UILabel!
    @IBOutlet weak var errorPhone : UILabel!
    @IBOutlet weak var errorEmail : UILabel!
    @IBOutlet weak var errorReason : UILabel!
    @IBOutlet weak var errormessage: UILabel!
    let model = ContactUsModel()



    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegates()
        addTargets()
    }
    
    func setDelegates() {
        nameField.delegate = self
        phoneField.delegate = self
        emailField.delegate = self
        reaseonField.delegate = self
        messageField.delegate = self
    }

    func addTargets() {
         nameField.addTarget(self, action:#selector(ContactUsViewController.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
         phoneField.addTarget(self, action: #selector(ContactUsViewController.textFieldDidChange(_:)),
                             for: UIControl.Event.editingChanged)
        emailField.addTarget(self, action: #selector(ContactUsViewController.textFieldDidChange(_:)),
                                    for: UIControl.Event.editingChanged)
        reaseonField.addTarget(self, action: #selector(ContactUsViewController.textFieldDidChange(_:)),
                                    for: UIControl.Event.editingChanged)
   }
    
    func validate() -> Bool {
        var isValid = true
        if !(nameField.text?.count ?? 0 > 0) {
            isValid = false
            errorName.isHidden = false
        }
        if !(phoneField.text?.count ?? 0 > 0) {
            isValid = false
            errorPhone.isHidden = false
        }
        if !(emailField.text?.count ?? 0 > 0) {
            isValid = false
            errorEmail.isHidden = false
        }
        if !(reaseonField.text?.count ?? 0 > 0){
            isValid = false
            errorReason.isHidden = false
        }
        if !(messageField.text?.count ?? 0 > 0){
            isValid = false
            errormessage.isHidden = false
        }
        return isValid
    }
    
    @IBAction func openReason(_sender : Any){
    
        let alert = UIAlertController(title: "سبب التواصل", message: "", preferredStyle: .actionSheet)

           alert.addAction(UIAlertAction(title: "اقتراح", style: .default , handler:{ (UIAlertAction)in
            self.model.contactUsType = 1
            self.errorReason.isHidden = true
            self.reaseonField.text = "اقتراح"
           }))

           alert.addAction(UIAlertAction(title: "شكوي", style: .default , handler:{ (UIAlertAction)in
            self.model.contactUsType = 2
            self.errorReason.isHidden = true
            self.reaseonField.text = "شكوي"
           }))

           alert.addAction(UIAlertAction(title: "استفسار", style: .default , handler:{ (UIAlertAction)in
            self.model.contactUsType = 3
            self.errorReason.isHidden = true
            self.reaseonField.text = "استفسار"
           }))
        alert.addAction(UIAlertAction(title: "سبب اخر", style: .default , handler:{ (UIAlertAction)in
         self.model.contactUsType = 4
            self.errorReason.isHidden = true
            self.reaseonField.text = "سبب اخر"
        }))
           self.present(alert, animated: true, completion: {
               print("completion block")
           })
    }
    
    @IBAction func sendAction(_sender :Any){
        if (!validate()){
            return
        }
        submitContactUs()
    }
    
    func submitContactUs() {
        
        model.email = emailField.text
        model.phone = phoneField.text
        model.msg = messageField.text
        model.name = nameField.text
        KVNProgress.show()
        let dataSource = GeneralDataSource()
        dataSource.submitContactUs(contactUsModel : model){ (status, response) in
            KVNProgress.dismiss()
            switch status {
            case .sucess :
                self.showMessage("تم ارسال طلبك بنجاح ، شكرا علي ثقتكم بنا") {self.dismiss(animated: true, completion: nil)}
                break
            case .error :
                self.showMessage(response as! String)
                break
            case .networkError:
                self.showMessage(response as! String)
                break
            }
        }
    }
    
}

extension ContactUsViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == nameField && (nameField.text?.count)! > 0 {
            errorName.isHidden = true
        }
        
        if textField == phoneField && (phoneField.text?.count)! > 0 {
            errorPhone.isHidden = true
        }
        if textField == emailField && (emailField.text?.count)! > 0 {
                   errorEmail.isHidden = true
        }
        if textField == reaseonField && (reaseonField.text?.count)! > 0 {
                          errorReason.isHidden = true
        }
    }
    
}

extension ContactUsViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == messageField && (messageField.text?.count)! > 0 {
                errormessage.isHidden = true
        }
    }
}
