//
//  MenuProtocol.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit


protocol ViewToPresenterMenuProtocol: class {
    
    var view : PresenterToViewMenuProtocol? {get set}
    var interactor : PresenterToIntetractorMenuProtocol? {get set}
    var router : PresenterToRouterMenuProtocol? {get set}
    
    var numberOfMenuElementsRows: Int { get }
    func configureMenuElementCell(cell: MenuTableViewCell, indexPath: IndexPath)
    func didSelectElement(itemIndex : Int)
    func populateSideMenuList(sideMenuItems: [SideMenu])
    func start()
}

protocol PresenterToViewMenuProtocol: class {
    func showPopUp(message : String)
    func reloadData()
    func navigate(viewController: UIViewController , animation : Bool)
}

protocol PresenterToIntetractorMenuProtocol: class {
    var presenter: InteractorToPresenterMenuProtocol? { get set }
    func getSideMenuItems()
    func navigate(item: SideMenu)
}

protocol PresenterToRouterMenuProtocol: class  {
    static func createModule() -> UIViewController
    func navigate(from sourceView: PresenterToViewMenuProtocol?, to destinationView: UIViewController, animation: Bool)
}

protocol InteractorToPresenterMenuProtocol: class {
    func populateSideMenuList(sideMenuItems: [SideMenu])
    func navigate(viewController: UIViewController , animation : Bool)
}
