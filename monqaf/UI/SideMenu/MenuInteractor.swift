//
//  MenuInteractor.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import BarcodeScanner

enum SideMenuKeys: String {
    case NEWS = "أخبار الجمعية"
    case SOCIAL_NEWS = "أخبار إجتماعية"
    case OFFERS = "عروض ومهرجانات"
    case BARCODE_READER = "قارئ الباركود"
    case PROFIT = "أرباح المساهمين"
    case REPORT = "إبلاغ عن سلعة"
    case BRANCHES = "الأفرع و الخدمات"
    case PROOF = "دليل الديوانيات"
    case GALLERY = "ألبوم الصور"
    case MANAGMENT = "مجلس الإدارة"
    case FAQs = "سؤال و جواب"
}

class MenuInteractor: PresenterToIntetractorMenuProtocol {
    
    var presenter: InteractorToPresenterMenuProtocol?
    
    func getSideMenuItemList() -> [SideMenu] {
        var sideMenuItems = [SideMenu]()
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.NEWS.rawValue, elementIcon : #imageLiteral(resourceName: "ic_menu_news.png")))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.SOCIAL_NEWS.rawValue, elementIcon :#imageLiteral(resourceName: "ic_menu_socialnews.png") ))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.OFFERS.rawValue, elementIcon : #imageLiteral(resourceName: "ic_menu_offers.png")))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.BARCODE_READER.rawValue, elementIcon :#imageLiteral(resourceName: "ic_menu_barcode.png") ))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.PROFIT.rawValue, elementIcon : #imageLiteral(resourceName: "ic_menu_profit.png")))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.REPORT.rawValue, elementIcon :#imageLiteral(resourceName: "ic_menu_report.png") ))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.BRANCHES.rawValue, elementIcon : #imageLiteral(resourceName: "ic_menu_branch.png")))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.PROOF.rawValue, elementIcon : #imageLiteral(resourceName: "ic_menu_location.png")))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.GALLERY.rawValue, elementIcon :#imageLiteral(resourceName: "ic_menu_gallery.png")))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.MANAGMENT.rawValue, elementIcon : #imageLiteral(resourceName: "ic_menu_managment.png")))
        sideMenuItems.append(SideMenu.init(elementText: SideMenuKeys.FAQs.rawValue, elementIcon : #imageLiteral(resourceName: "ic_menu_faqs.png")))
        return sideMenuItems
    }
    
    func getSideMenuItems() {
        presenter?.populateSideMenuList(sideMenuItems: self.getSideMenuItemList())
    }
    
    func navigate(item: SideMenu) {
        switch item.elementText {
        case SideMenuKeys.NEWS.rawValue:
            presenter?.navigate(viewController: NewsRouter.createModule(title: SideMenuKeys.NEWS.rawValue,fromWhere: .NEWS), animation: true)
            break
        case SideMenuKeys.SOCIAL_NEWS.rawValue:
            presenter?.navigate(viewController: NewsRouter.createModule(title: SideMenuKeys.SOCIAL_NEWS.rawValue,fromWhere: .SOCIAL_NEWS), animation: true)
            break
        case SideMenuKeys.OFFERS.rawValue:
            presenter?.navigate(viewController: NewsRouter.createModule(title: SideMenuKeys.OFFERS.rawValue,fromWhere: .OFFERS), animation: true)
            break
        case SideMenuKeys.BARCODE_READER.rawValue:
            presenter?.navigate(viewController: BarcodeScannerViewController(), animation: true)
            break
        case SideMenuKeys.PROFIT.rawValue:
            let vc = ProfitsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
            presenter?.navigate(viewController:vc , animation: true)
            break
        case SideMenuKeys.REPORT.rawValue:
            let vc = ReportProductViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
                       presenter?.navigate(viewController:vc , animation: true)
            break
        case SideMenuKeys.BRANCHES.rawValue:
            let vc = BranchesViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
            presenter?.navigate(viewController: vc, animation: true)
            break
        case SideMenuKeys.PROOF.rawValue:
            presenter?.navigate(viewController: ProofRouter.createModule(), animation: true)
            break
        case SideMenuKeys.GALLERY.rawValue:
            presenter?.navigate(viewController: NewsRouter.createModule(title: SideMenuKeys.GALLERY.rawValue,fromWhere: .MEDIA), animation: true)
            break
        case SideMenuKeys.MANAGMENT.rawValue:
            presenter?.navigate(viewController: BoardMemberRouter.createModule() , animation: true)
            break
        case SideMenuKeys.FAQs.rawValue:
            presenter?.navigate(viewController:
                QuestionsAnswersViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu),
                                animation: true)
            break
        default:
            break
        }
    }
    
    
}
