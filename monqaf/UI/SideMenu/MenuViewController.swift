//
//  MenuViewController.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    
    var mainViewController: UIViewController!
    var presenter : ViewToPresenterMenuProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.start()
    }
    
    @IBAction func openInstagram(){
        Utils.openWebsite(link: "https://instagram.com/almangaf_co_op?")
    }
    
    @IBAction func openTwitter(){
           Utils.openWebsite(link: "https://mobile.twitter.com/MNF_Coop")
       }
}

extension MenuViewController : PresenterToViewMenuProtocol{
    
    func showPopUp(message: String) {
        self.showMessage(message)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func navigate(viewController: UIViewController, animation: Bool) {
        self.present(viewController, animated: animation, completion: nil)
    }
}

extension MenuViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = (presenter?.numberOfMenuElementsRows ?? 0)
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.presenter?.didSelectElement(itemIndex: indexPath.row)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        self.presenter?.configureMenuElementCell(cell: cell, indexPath: indexPath)
        return cell
    }
}
