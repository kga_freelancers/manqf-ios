//
//  NewsRouter.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class NewsRouter : PresenterToRouterNewsProtocol {
 
    static func createModule(title:String,fromWhere:NewsKeys) -> UIViewController {
        
        let view = NewsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        let presenter : ViewToPresenterNewsProtocol & InteractorToPresenterNewsProtocol = NewsPresenter()
        let interactor : PresenterToIntetractorNewsProtocol = NewsInteractor()
        let router : PresenterToRouterNewsProtocol = NewsRouter()
        
        view.presenter = presenter
        view.toolbarTitle = title
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        presenter.fromWhere = fromWhere
        interactor.presenter = presenter
        return view
    }
    
    func navigate(from sourceView: PresenterToViewNewsProtocol?, to destinationView: UIViewController, animation: Bool) {

        if let sourceView = sourceView as? UIViewController {
            sourceView.present(destinationView, animated: true, completion: nil)
        }
    }

    
}
