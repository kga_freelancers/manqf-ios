//
//  NewsTableViewCell.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsDateLabel : UILabel!
    @IBOutlet weak var newsTitleLabel : UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setData(news:News){
        newsImageView.sd_setImage(with: URL(string: news.imageURL?.getCloudNariImageUrl() ?? "")) { (image, error, type, url) in
        }
        var mDate = news.date ?? ""
        let indexStartOfText = mDate.index(mDate.startIndex, offsetBy: 10)
        mDate =  String(mDate[..<indexStartOfText])
        newsDateLabel.text = mDate.getDate()
        newsTitleLabel.text = news.title ?? ""
    }
    func setOfferData(offer:Offer){
        if((offer.images ?? [String]()).count > 0){
            newsImageView.sd_setImage(with: URL(string: offer.images?[0].getCloudNariImageUrl() ?? "")) { (image, error, type, url) in
            }
        }
        var mDate = offer.date ?? ""
        let indexStartOfText = mDate.index(mDate.startIndex, offsetBy: 10)
        mDate =  String(mDate[..<indexStartOfText])
        
        var toDate = offer.toDate ?? ""
        if(!toDate.isEmpty && toDate.count >= 10){
            let indexStartOfToText = toDate.index(toDate.startIndex, offsetBy: 10)
            toDate =  String(toDate[..<indexStartOfToText])
        }

        if(!toDate.isEmpty && toDate.count >= 10){
            newsDateLabel.text = " من " + mDate.getDate() + " الي " + toDate.getDate()
        }
        else{
             newsDateLabel.text = mDate.getDate()
        }

        newsTitleLabel.text = offer.title ?? ""
    }

    func setGalleryData(offer:Offer){
        if((offer.images ?? [String]()).count > 0){
            newsImageView.sd_setImage(with: URL(string: offer.images?[0].getCloudNariImageUrl() ?? "")) { (image, error, type, url) in
            }
        }
        var mDate = offer.date ?? ""
        let indexStartOfText = mDate.index(mDate.startIndex, offsetBy: 10)
        mDate =  String(mDate[..<indexStartOfText])
        newsDateLabel.text = mDate.getDate()
        newsTitleLabel.text = offer.name ?? ""
    }
}
