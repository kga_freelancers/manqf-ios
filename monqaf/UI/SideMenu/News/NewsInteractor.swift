//
//  NewsInteractor.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class NewsInteractor: PresenterToIntetractorNewsProtocol {
    var fetchedOffers = [Offer]()
    var allOffers = [Offer]()
    var presenter: InteractorToPresenterNewsProtocol?
    
    func loadNews() {
        let dataSource = GeneralDataSource()
        dataSource.loadNews { (status, response) in
            switch status {
            case .sucess :
                self.presenter?.populateNewsList(newsList: (response as? NewsResponse)?.newsList?.list ?? [News]())
                break
            case .error :
                self.presenter?.showError(message: response as? String ?? "")
                break
            case .networkError:
                self.presenter?.showError(message: response as? String ?? "")
                break
            }
        }
    }
    
    func loadSocialNews() {
        let dataSource = GeneralDataSource()
        dataSource.loadSocialNews { (status, response) in
            switch status {
            case .sucess :
                self.presenter?.populateNewsList(newsList: (response as? NewsResponse)?.newsList?.list ?? [News]())
                break
            case .error :
                self.presenter?.showError(message: response as? String ?? "")
                break
            case .networkError:
                self.presenter?.showError(message: response as? String ?? "")
                break
            }
        }
    }
    
    func loadOffers(page:Int) {
         let dataSource = GeneralDataSource()
           dataSource.loadOffers(pageNumber: page) {(status, response) in
               switch status {
               case .sucess :
                   let respone =  (response as? OffersResponse)
                   self.fetchedOffers = respone?.offersList?.list ?? [Offer]()
                   self.handleOffersPagination()
                   break
               case .error :
                   self.presenter?.showError(message: response as? String ?? "")
                   break
               case .networkError:
                   self.presenter?.showError(message: response as? String ?? "")
                   break
               }
           }
    }
    
    func handleOffersPagination()  {
        if self.fetchedOffers.count < Constants.PER_PAGE {
            self.presenter?.stopPagination()
        }
        allOffers.append(contentsOf: self.fetchedOffers)
        self.presenter?.populateOffersList(newsList:allOffers)
    }


    func emptyAllOffers(){
        allOffers = []
    }
    
    func loadGallery() {
         let dataSource = GeneralDataSource()
           dataSource.loadGallery { (status, response) in
               switch status {
               case .sucess :
                   self.presenter?.populateOffersList(newsList: (response as? OffersResponse)?.offersList?.list ?? [Offer]())
                   break
               case .error :
                   self.presenter?.showError(message: response as? String ?? "")
                   break
               case .networkError:
                   self.presenter?.showError(message: response as? String ?? "")
                   break
               }
           }
    }
    
    

}
