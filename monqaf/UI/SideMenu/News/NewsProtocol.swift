//
//  NewsProtocol.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

enum NewsKeys: String {
    case NEWS = "News"
    case SOCIAL_NEWS = "SoicalNews"
    case OFFERS = "Offers"
    case MEDIA = "Media"
}
protocol ViewToPresenterNewsProtocol: class {
    
    var view : PresenterToViewNewsProtocol? {get set}
    var interactor : PresenterToIntetractorNewsProtocol? {get set}
    var router : PresenterToRouterNewsProtocol? {get set}
    
    var  page : Int { get }
    var  numberOfItems: Int { get }
    var  fromWhere : NewsKeys? {get set}
    func loadPagingData(indexPath : IndexPath)
    func configureNewsCell(cell: NewsTableViewCell, indexPath: IndexPath)
    func didSelectElement(itemIndex : Int)
    func populateNewsList(newsList: [News])
    func populateOffersList(newsList: [Offer])
    func loadData()
}

protocol PresenterToViewNewsProtocol: class {
    func showError(message : String)
    func reloadData()
}

protocol PresenterToIntetractorNewsProtocol: class {
    var presenter: InteractorToPresenterNewsProtocol? { get set }
    func loadNews()
    func loadSocialNews()
    func loadOffers(page:Int)
    func loadGallery()
}

protocol PresenterToRouterNewsProtocol: class  {
    static func createModule(title:String,fromWhere:NewsKeys) -> UIViewController
    func navigate(from sourceView: PresenterToViewNewsProtocol?, to destinationView: UIViewController, animation: Bool)
}

protocol InteractorToPresenterNewsProtocol: class {
    func populateNewsList(newsList: [News])
    func populateOffersList(newsList: [Offer])
    func showError(message : String)
    func stopPagination()
}
