//
//  NewsViewController.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit


class NewsViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var loading : UIActivityIndicatorView!
    
    var presenter : ViewToPresenterNewsProtocol?
    var toolbarTitle = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLoading()
        self.presenter?.loadData()
        self.titleLabel.text = self.toolbarTitle
    }
    
    private func showLoading(){
        self.loading.isHidden = false
        self.loading.startAnimating()
    }
    private func hideLoading(){
        self.loading.isHidden = true
        self.loading.stopAnimating()
    }


}
extension NewsViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter?.numberOfItems ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell") as! NewsTableViewCell
        self.presenter?.configureNewsCell(cell: cell, indexPath: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.didSelectElement(itemIndex: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.presenter?.loadPagingData(indexPath: indexPath)
    }
}
extension NewsViewController : PresenterToViewNewsProtocol{
    func showError(message: String) {
        self.hideLoading()
        self.showMessage(message)
    }
    
    func reloadData() {
        self.hideLoading()
        self.tableView.reloadData()
    }
    
}
