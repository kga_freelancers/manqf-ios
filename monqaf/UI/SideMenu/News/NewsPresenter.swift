//
//  NewsPresenter.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class NewsPresenter: ViewToPresenterNewsProtocol {
    
    var newsList = [News]()
    var socialNewsList = [News]()
    var offersList = [Offer]()
    var view: PresenterToViewNewsProtocol?
    var interactor: PresenterToIntetractorNewsProtocol?
    var router: PresenterToRouterNewsProtocol?

    var fromWhere: NewsKeys?
    var page: Int = 0
    var isFinishedPaging = false

    var numberOfItems: Int{
        switch fromWhere {
        case .NEWS?:
            return self.newsList.count
        case .SOCIAL_NEWS?:
            return self.socialNewsList.count
        case .OFFERS?:
            return self.offersList.count
        case .MEDIA?:
            return self.offersList.count
        default:
            return 0
        }
    }
    
    func configureNewsCell(cell: NewsTableViewCell, indexPath: IndexPath) {
        switch fromWhere {
        case .NEWS?:
                  cell.setData(news: self.newsList[indexPath.row])
                break
        case .SOCIAL_NEWS?:
                  cell.setData(news: self.socialNewsList[indexPath.row])
                break
        case .OFFERS?:
                  cell.setOfferData(offer: self.offersList[indexPath.row])
                break
        case .MEDIA?:
                cell.setGalleryData(offer: self.offersList[indexPath.row])
                break
               default:
                break
        }
    }
    
    func didSelectElement(itemIndex: Int) {
        switch fromWhere {
        case .NEWS?:
              let vc = OfferDetailsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
              vc.headerTitle = "اخبار الجمعية"
              vc.news = newsList[itemIndex]
              self.router?.navigate(from: self.view, to: vc, animation: true)
              break
        case .SOCIAL_NEWS?:
             let vc = DetailsViewController.instantiateFromStoryBoard(appStoryBoard: .Main)
             vc.headerTitle = "اخبار اجتماعيه"
             vc.news = socialNewsList[itemIndex]
             self.router?.navigate(from: self.view, to: vc, animation: true)
            break
        case .OFFERS?:
            let vc = OfferDetailsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
            vc.headerTitle = "عروض ومهرجانات"
            vc.offer = offersList[itemIndex]
            self.router?.navigate(from: self.view , to: vc , animation: true)
            break
        case .MEDIA?:
            let vc = GalleryDetailsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
            vc.media = self.offersList[itemIndex]
            self.router?.navigate(from: self.view, to: vc, animation: true)
            break
          default:
            break
        }
    }
    
    func loadData() {
        switch fromWhere {
        case .NEWS?:
             self.interactor?.loadNews()
            break
        case .SOCIAL_NEWS?:
            self.interactor?.loadSocialNews()
            break
        case .OFFERS?:
            let count = offersList.count
            self.page =  ( count / Constants.PER_PAGE ) //+ 1
            self.interactor?.loadOffers(page: page)
            break
        case .MEDIA?:
           self.interactor?.loadGallery()
            break
          default:
           break
        }
    }
    
    func loadPagingData(indexPath: IndexPath) {
        let count = offersList.count
        let offersCount = count 
        if indexPath.row == offersCount && !isFinishedPaging {
            self.loadData()
        }
    }

}
extension NewsPresenter : InteractorToPresenterNewsProtocol{
    func showError(message: String) {
        self.view?.showError(message: message)
    }
    
    func populateNewsList(newsList: [News]) {
        if(fromWhere == .NEWS){
            self.newsList = newsList
            self.view?.reloadData()
        }
        else if(fromWhere == .SOCIAL_NEWS){
            self.socialNewsList = newsList
            self.view?.reloadData()
        }
    }

    func populateOffersList(newsList: [Offer]) {
        self.offersList = newsList
        self.view?.reloadData()
    }
    
    
    func stopPagination() {
        isFinishedPaging = true
    }
    

}
