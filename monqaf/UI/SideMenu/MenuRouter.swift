//
//  MenuRouter.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit


class MenuRouter : PresenterToRouterMenuProtocol {
    
    static func createModule() -> UIViewController {
        
        let view = MenuViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        let presenter : ViewToPresenterMenuProtocol & InteractorToPresenterMenuProtocol = MenuPresenter()
        let interactor : PresenterToIntetractorMenuProtocol = MenuInteractor()
        let router : PresenterToRouterMenuProtocol = MenuRouter()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        interactor.presenter = presenter
        return view
    }
    
    func navigate(from sourceView: PresenterToViewMenuProtocol?, to destinationView: UIViewController, animation: Bool) {
        
    }
    
    
}
