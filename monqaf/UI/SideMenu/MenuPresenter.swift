//
//  MenuPresenter.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import BarcodeScanner

class MenuPresenter : ViewToPresenterMenuProtocol {
    var sideMenuItems = [SideMenu]()
    var view: PresenterToViewMenuProtocol?
    var interactor: PresenterToIntetractorMenuProtocol?
    var router: PresenterToRouterMenuProtocol?
    
    
    var numberOfMenuElementsRows: Int{
        return self.sideMenuItems.count
    }
    
    func start() {
        interactor?.getSideMenuItems()
    }
    
    func populateSideMenuList(sideMenuItems: [SideMenu]) {
        self.sideMenuItems = sideMenuItems
        self.view?.reloadData()
    }
    func configureMenuElementCell(cell: MenuTableViewCell, indexPath: IndexPath) {
        cell.setData(sideMenu: sideMenuItems[indexPath.row])
    }
    
    func didSelectElement(itemIndex: Int) {
        self.interactor?.navigate(item: sideMenuItems[itemIndex])
    }
}

extension MenuPresenter : InteractorToPresenterMenuProtocol {
    func navigate(viewController: UIViewController, animation: Bool) {
        if(viewController is BarcodeScannerViewController){
            (viewController as? BarcodeScannerViewController)?.isOneTimeSearch = true
            (viewController as? BarcodeScannerViewController)?.codeDelegate = self
            (viewController as? BarcodeScannerViewController)?.errorDelegate = self
            (viewController as? BarcodeScannerViewController)?.dismissalDelegate = self
            self.view?.navigate(viewController: viewController, animation: animation)
        }
        else{
            self.view?.navigate(viewController: viewController, animation: animation)
        }
    }
    
}

extension MenuPresenter : BarcodeScannerCodeDelegate{
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print(code)
        controller.dismiss(animated: true) {
            self.view?.navigate(viewController: BarcodeResultRouter.createModule(barcode: code), animation: true)
        }
    }
    
    
}
extension MenuPresenter : BarcodeScannerErrorDelegate{
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
//        controller.resetWithError(message: "لا يوجد منتج بهذه التفاصيل .. حاول مجددا")
        print(error.localizedDescription)
    }
    
    
}
extension MenuPresenter : BarcodeScannerDismissalDelegate{
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
            controller.dismiss(animated: true, completion: nil)
    }
}

