//
//  MenuTableViewCell.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemTextLabel : UILabel!
    @IBOutlet weak var itemImage : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(sideMenu : SideMenu){
        itemTextLabel.text = sideMenu.elementText
        itemImage.image = sideMenu.elementIcon
    }

}
