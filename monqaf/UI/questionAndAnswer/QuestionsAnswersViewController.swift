//
//  QuestionsAnswers.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import KVNProgress

class QuestionsAnswersViewController: UIViewController {
    
    @IBOutlet weak var questionImageView : UIImageView!
    @IBOutlet weak var questionLabel : UILabel!
    @IBOutlet weak var prizeLabel : UILabel!
    @IBOutlet weak var answerField : UITextField!
    @IBOutlet weak var nameField : UITextField!
    @IBOutlet weak var phoneField : UITextField!
    @IBOutlet weak var boxField : UITextField!
    @IBOutlet weak var answerErrorLabel : UILabel!
    @IBOutlet weak var nameErrorLabel : UILabel!
    @IBOutlet weak var phoneErrorLabel : UILabel!
    @IBOutlet weak var boxErrorLabel : UILabel!
    
    var question  = Question()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupFields()
        self.loadQuestions()
    }
    
    func setupFields() {
          answerField.addTarget(self, action:#selector(QuestionsAnswersViewController.textFieldDidChange(_:)),
                                    for: UIControl.Event.editingChanged)
          nameField.addTarget(self, action: #selector(QuestionsAnswersViewController.textFieldDidChange(_:)),
                              for: UIControl.Event.editingChanged)
          phoneField.addTarget(self, action: #selector(QuestionsAnswersViewController.textFieldDidChange(_:)),
            for: UIControl.Event.editingChanged)
          boxField.addTarget(self, action: #selector(QuestionsAnswersViewController.textFieldDidChange(_:)),
            for: UIControl.Event.editingChanged)
        
          answerField.delegate = self
          nameField.delegate = self
          phoneField.delegate = self
          boxField.delegate = self
      }
    
    private func setViewData(){
        self.questionImageView.sd_setImage(with: URL(string: question.imageURL?.getCloudNariImageUrl() ?? ""))
        self.questionLabel.text = question.title
        self.prizeLabel.text = question.questionDescription
    }
    
    func validate() -> Bool {
       var isValid = true
       if answerField.text?.isEmpty ?? false {
           isValid = false
           answerErrorLabel.isHidden = false
       }
       
       if nameField.text?.isEmpty ?? false {
           isValid = false
           nameErrorLabel.isHidden = false
       }
        if phoneField.text?.isEmpty ?? false {
            isValid = false
            phoneErrorLabel.isHidden = false
        }
        if boxField.text?.isEmpty ?? false {
            isValid = false
            boxErrorLabel.isHidden = false
        }
       return isValid
    }
    
    @IBAction func submitAnswerClick(_ sender:Any){
        if(!validate()){
            return
        }
        self.view.endEditing(true)
        self.submitAnswer()
    }
    
    private func loadQuestions(){
        KVNProgress.show()
        let dataSource = GeneralDataSource()
            dataSource.loadQuestions { (status, response) in
                KVNProgress.dismiss()
                switch status {
                case .sucess :
                    if(((response as? QuestionResponse)?.questionList?.list ?? [Question]()).count > 0){
                        self.question = ((response as? QuestionResponse)?.questionList?.list ?? [Question]())[0]
                        self.setViewData()
                    }
                    break
                case .error :
                    self.showMessage(response as? String ?? "")
                    break
                case .networkError:
                    self.showMessage(response as? String ?? "")
                    break
                }
            }
    }
    
    private func submitAnswer(){
           KVNProgress.show()
            let dataSource = GeneralDataSource()
            let mAnswer = Answer()
            mAnswer.questionID = question.id ?? 0
            mAnswer.answer = answerField.text ?? ""
            mAnswer.username = nameField.text ?? ""
            mAnswer.boxNumber = boxField.text ?? ""
            mAnswer.phone = phoneField.text ?? ""
        dataSource.submitAnswer(answer: mAnswer) { (status, response) in
            KVNProgress.dismiss()
            switch status {
            case .sucess :
                self.showMessage("تم ارسال إجابتك بنجاح . نتمني لكم وافر الحظ") {
                    self.setupHomeWithSideMenu()
                }
                break
            case .error :
                self.showMessage(response as? String ?? "")
                break
            case .networkError:
                self.showMessage(response as? String ?? "")
                break
            }
        }
    }
}

extension QuestionsAnswersViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == answerField && (answerField.text?.count)! > 0 {
            answerErrorLabel.isHidden = true
        }
        
        if textField == nameField && (nameField.text?.count)! > 0 {
            nameErrorLabel.isHidden = true
        }
        
        if textField == phoneField && (phoneField.text?.count)! > 0 {
            phoneErrorLabel.isHidden = true
        }
        
        if textField == boxField && (boxField.text?.count)! > 0 {
            boxErrorLabel.isHidden = true
        }
    }
    
}
