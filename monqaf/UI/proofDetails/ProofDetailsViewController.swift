//
//  ProofDetailsViewController.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class ProofDetailsViewController: UIViewController {
    
    @IBOutlet weak var proofImageView : UIImageView!
    @IBOutlet weak var proofNameLabel : UILabel!
    @IBOutlet weak var proofPhoneNumberLabel : UILabel!
    @IBOutlet weak var proofNotesLabel : UILabel!
    @IBOutlet weak var proofLocationImageView : UIImageView!
    @IBOutlet weak var proofAddressLabel : UILabel!
    
    var proof = Proof()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViewData()
    }

    private func setViewData(){
        proofNameLabel.text = proof.name
        proofPhoneNumberLabel.text = proof.phone
        proofNotesLabel.text = proof.notes
        proofAddressLabel.text = proof.address
    }
    
    @IBAction func call(_ sender:Any){
        Utils.call(number: proof.phone ?? "")
    }
    
    @IBAction func navigate (_ sender:Any){
        Utils.navigateToMap(latitude: "\(proof.lat ?? 0.0)", longitude: "\(proof.lng ?? 0.0)")
    }
    
    @IBAction func share (_ sender:Any){
         let myLink = ((proof.name ?? "") + ((":" + (proof.address ?? "")) + ("لمتابعة اخر الأخبار المتعلقة بالجمعيه حمل تطبيق جمعيه المنقف من خلال هذا الرابط")))
        Utils.share(vc: self, link: myLink)
    }
}
