//
//  BarcodeResultViewController.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class BarcodeResultViewController: UIViewController {
    
    @IBOutlet weak var nodataView : UIView!
    @IBOutlet weak var loading : UIActivityIndicatorView!
    @IBOutlet weak var detailsView : UIView!
    @IBOutlet weak var productImageView : UIImageView!
    @IBOutlet weak var productNameLabel : UILabel!
    @IBOutlet weak var productPriceLabel : UILabel!
    @IBOutlet weak var productCategoryLabel : UILabel!
    @IBOutlet weak var productSubcategoryLabel : UILabel!
    
    var presenter : ViewToPresenterBarcodeProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if presenter?.product.id != -1 {
            hideLoading()
            showViewData()
            setViewData(data: presenter?.product ?? Barcode())
        }else{
            self.showLoading()
            self.presenter?.loadBarcodeResult()
        }
    }
    
    private func showLoading(){
        self.loading.isHidden = false
        self.loading.startAnimating()
    }
    private func hideLoading(){
        self.loading.isHidden = true
        self.loading.stopAnimating()
    }
    
    private func showViewData(){
        self.nodataView.isHidden = true
        self.detailsView.isHidden = false
    }
    
    private func hideViewData(){
        self.nodataView.isHidden = false
        self.detailsView.isHidden = true
    }
    
    private func setViewData(data: Barcode){
        self.productImageView.sd_setImage(with: URL(string: data.imageURL?.getCloudNariImageUrl() ?? "")) { (image, error, type, url) in
        }
        self.productNameLabel.text = data.shortName ?? ""
        self.productPriceLabel.text = "\(data.price ?? 0.0)"
        self.productCategoryLabel.text = data.categoryName ?? ""
        self.productSubcategoryLabel.text = data.subCategoryName ?? ""
    }
    
}
extension BarcodeResultViewController : PresenterToViewBarcodeProtocol{
    func showBarcodeResult(data: BarcodeResponse) {
        self.hideLoading()
        self.showViewData()
        self.setViewData(data: data.barcodeData?.barCode ?? Barcode())
    }
    
    func showBacodeError(error: String) {
        self.hideLoading()
        self.hideViewData()
    }
    
    
}
