//
//  BarcodeResultPresenter.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class BarcodeResultPresenter : ViewToPresenterBarcodeProtocol {
    
    var product = Barcode()
    var view: PresenterToViewBarcodeProtocol?
    var interactor: PresenterToIntetractorBarcodeProtocol?
    var router: PresenterToRouterBarcodeProtocol?
    
    var barcode = ""
    
    func loadBarcodeResult() {
        self.interactor?.loadBarcodeResult(barcode: barcode)
    }
    
}

extension BarcodeResultPresenter : InteractorToPresenterBarcodeProtocol{
    func showBarcodeResult(data: BarcodeResponse) {
        self.view?.showBarcodeResult(data: data)
    }
    
    func showBacodeError(error: String) {
        self.view?.showBacodeError(error: error)
    }
    
    
}
