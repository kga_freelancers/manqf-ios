//
//  BarcodeResultProtocol.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterBarcodeProtocol: class {
    
    var view : PresenterToViewBarcodeProtocol? {get set}
    var interactor : PresenterToIntetractorBarcodeProtocol? {get set}
    var router : PresenterToRouterBarcodeProtocol? {get set}
    
    var barcode: String { get set}
    var product : Barcode {get set}
    
    func loadBarcodeResult()
    
}

protocol PresenterToViewBarcodeProtocol: class {
    func showBarcodeResult(data : BarcodeResponse)
    func showBacodeError(error : String)
}

protocol PresenterToIntetractorBarcodeProtocol: class {
    var presenter: InteractorToPresenterBarcodeProtocol? { get set }
    
    func loadBarcodeResult(barcode:String)
    
}

protocol PresenterToRouterBarcodeProtocol: class  {
    static func createModule(barcode:String) -> UIViewController
}

protocol InteractorToPresenterBarcodeProtocol: class {
    func showBarcodeResult(data : BarcodeResponse)
    func showBacodeError(error : String)
}


