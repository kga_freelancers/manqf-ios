//
//  BarcodeResultRouter.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit


class BarcodeResultRouter : PresenterToRouterBarcodeProtocol {
   
    static func createModule(barcode: String) -> UIViewController {
        let view = BarcodeResultViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
            let presenter : ViewToPresenterBarcodeProtocol & InteractorToPresenterBarcodeProtocol = BarcodeResultPresenter()
            let interactor : PresenterToIntetractorBarcodeProtocol = BarcodeResultInteractor()
            let router : PresenterToRouterBarcodeProtocol = BarcodeResultRouter()
            
            view.presenter = presenter
            presenter.interactor = interactor
            presenter.barcode = barcode
            presenter.view = view
            presenter.router = router
            interactor.presenter = presenter
            return view
    }
    
    static func createModule(product: Barcode) -> UIViewController {
           let view = BarcodeResultViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
               let presenter : ViewToPresenterBarcodeProtocol & InteractorToPresenterBarcodeProtocol = BarcodeResultPresenter()
               let interactor : PresenterToIntetractorBarcodeProtocol = BarcodeResultInteractor()
               let router : PresenterToRouterBarcodeProtocol = BarcodeResultRouter()
               
               view.presenter = presenter
               presenter.interactor = interactor
               presenter.product = product
               presenter.view = view
               presenter.router = router
               interactor.presenter = presenter
               return view
       }

}

