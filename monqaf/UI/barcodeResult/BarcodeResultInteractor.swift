//
//  BarcodeResultInteractor.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit


class BarcodeResultInteractor: PresenterToIntetractorBarcodeProtocol {

    var presenter: InteractorToPresenterBarcodeProtocol?

    func loadBarcodeResult(barcode: String) {
        let dataSource = GeneralDataSource()
        dataSource.loadBarcodeResult(barcode: barcode) { (status, response) in
            switch status {
              case .sucess :
                self.presenter?.showBarcodeResult(data: response as? BarcodeResponse ?? BarcodeResponse())
                  break
              case .error :
                self.presenter?.showBacodeError(error: response as? String ?? "")
                  break
              case .networkError:
                  self.presenter?.showBacodeError(error: response as? String ?? "")
                  break
              }
        }
    }
    
}
