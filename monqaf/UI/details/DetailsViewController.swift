//
//  DetailsViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import SDWebImage

class DetailsViewController: UIViewController {

    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var dateLabl : UILabel!
    @IBOutlet weak var titleLabl : UILabel!
    @IBOutlet weak var descriptionLabl : UILabel!
    @IBOutlet weak var headerLabl : UILabel!
    
    var news = News()
    var headerTitle = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    }
    
    func setData()  {
        imageView.sd_setImage(with: URL.init(string: news.imageURL?.getCloudNariImageUrl() ?? ""), completed: nil)
        var mDate = news.date ?? ""
        let indexStartOfText = mDate.index(mDate.startIndex, offsetBy: 10)
        mDate =  String(mDate[..<indexStartOfText])
        dateLabl.text = mDate.getDate()
        titleLabl.text = news.title
        descriptionLabl.text = news.newsDescription
        headerLabl.text = headerTitle
    }
}
