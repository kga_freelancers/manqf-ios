//
//  BranchesRouter.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

class BranchesRouter: PresenterToRouterBranchesProtocol {
    
    static func createInvestoresModule() -> UIViewController {
        
        let view = InvestorsViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        let presenter : ViewToPresenterBranchesProtocol & InteractorToPresenterBranchesProtocol = BranchesPresenter()
        
        let interactor : PresenterToIntetractorBranchesProtocol = BranchesInteractor()
        let router : PresenterToRouterBranchesProtocol = BranchesRouter()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        interactor.presenter = presenter
        return view
    }
    
    static func createCharityModule() -> UIViewController {
        
        let view = CharityViewController.instantiateFromStoryBoard(appStoryBoard: .SideMenu)
        let presenter : ViewToPresenterBranchesProtocol & InteractorToPresenterBranchesProtocol = BranchesPresenter()
        
        let interactor : PresenterToIntetractorBranchesProtocol = BranchesInteractor()
        let router : PresenterToRouterBranchesProtocol = BranchesRouter()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        interactor.presenter = presenter
        return view
    }
}
