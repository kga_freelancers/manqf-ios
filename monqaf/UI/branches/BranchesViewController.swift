//
//  ProfitsViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class BranchesViewController: ButtonBarPagerTabStripViewController {
    var charityVc = UIViewController()
    var investorsVc = UIViewController()
    
    override func viewDidLoad() {
        setUpTabBar()
        super.viewDidLoad()
    }
    
    func setUpTabBar()  {
        
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        settings.style.selectedBarBackgroundColor = UIColor.white
        settings.style.selectedBarHeight = 4.0
        settings.style.selectedBarBackgroundColor = Colors.pagerSelectedBarColor
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            let _ = self
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.black.withAlphaComponent(0.61)
            newCell?.label.textColor = UIColor.black
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        charityVc = BranchesRouter.createCharityModule()
        investorsVc = BranchesRouter.createInvestoresModule()
        return [investorsVc,charityVc]
    }
  

}
