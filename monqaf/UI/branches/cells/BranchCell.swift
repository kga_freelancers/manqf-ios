//
//  BranchCell.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

class BranchCell: UITableViewCell {
    
    @IBOutlet weak var branchImageView: UIImageView!
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var locationsLabel : UILabel!
    
    
    func setData(branch : Branch)  {
        branchImageView.sd_setImage(with: URL(string: branch.imageUrl?.getCloudNariImageUrl() ?? "")) { (image, error, type, url) in
        }
        
        nameLabel.text = branch.name ?? ""
        locationsLabel.text = branch.address ?? ""

    }
    
    
}
