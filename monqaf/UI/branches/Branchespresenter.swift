//
//  Branchespresenter.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class BranchesPresenter : ViewToPresenterBranchesProtocol {

    var view: PresenterToViewBranchesProtocol?
    var interactor: PresenterToIntetractorBranchesProtocol?
    var router: PresenterToRouterBranchesProtocol?
    
    func loadBranches(type :  Int) {
        interactor?.loadBranches(type: type)
    }
    
}
extension BranchesPresenter : InteractorToPresenterBranchesProtocol{
    
    func showData(data: BranchesResponse) {
        view?.showHomeData(data: data)
    }
    
    func showError(error: String) {
        view?.showError(error: error)
    }
}
