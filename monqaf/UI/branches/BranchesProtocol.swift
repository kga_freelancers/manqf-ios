//
//  BranchesProtocol.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit


protocol ViewToPresenterBranchesProtocol: class {
    
    var view : PresenterToViewBranchesProtocol? {get set}
    var interactor : PresenterToIntetractorBranchesProtocol? {get set}
    var router : PresenterToRouterBranchesProtocol? {get set}
    func loadBranches(type : Int)
}

protocol PresenterToViewBranchesProtocol: class {
    func showHomeData(data : BranchesResponse)
    func showError(error : String)
}

protocol PresenterToIntetractorBranchesProtocol: class {
    var presenter: InteractorToPresenterBranchesProtocol? { get set }
    
    func loadBranches(type : Int)

}

protocol PresenterToRouterBranchesProtocol: class  {
    static func createInvestoresModule() -> UIViewController
    static func createCharityModule() -> UIViewController

    
}

protocol InteractorToPresenterBranchesProtocol: class {
    func showData(data : BranchesResponse)
    func showError(error : String)
}
