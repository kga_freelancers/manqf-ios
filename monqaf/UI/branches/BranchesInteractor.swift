//
//  BranchesInteractor.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class BranchesInteractor : PresenterToIntetractorBranchesProtocol {
   
    
    var presenter: InteractorToPresenterBranchesProtocol?
  
    func loadBranches(type : Int) {
        let dataSource = GeneralDataSource()
        dataSource.loadBranches(typ : type) { (status, response) in
            switch status {
            case .sucess :
                let nranchesResponse = (response as? BranchesResponse
                ?? BranchesResponse())
                self.presenter?.showData(data: response as? BranchesResponse
                    ?? BranchesResponse())
                break
            case .error :
                self.presenter?.showError(error: response as? String ?? "")
                break
            case .networkError:
                self.presenter?.showError(error: response as? String ?? "")
                break
            }
        }
    }
}
