//
//  InvestorsViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class InvestorsViewController: UIViewController {

    var presenter : ViewToPresenterBranchesProtocol?
    @IBOutlet weak var indicator : UIActivityIndicatorView!
    @IBOutlet weak var tableView : UITableView!
    
    var branchesList  = [Branch]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegate()
        presenter?.loadBranches(type: 2)
        indicator.startAnimating()
    }
    
    func setDelegate() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    
}

extension InvestorsViewController : PresenterToViewBranchesProtocol {
    func showHomeData(data: BranchesResponse) {
        self.branchesList = data.result.brabchList ?? [Branch]()
        indicator.isHidden = true
        self.tableView.reloadData()
    }
    
    func showError(error: String) {
        indicator.isHidden = true
        self.showMessage(error)
    }
    
    
}
extension InvestorsViewController : IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "الفروع المستثمرة")
    }
}

extension InvestorsViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branchesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BranchCell") as! BranchCell
        cell.setData(branch: branchesList[indexPath.row])
        return cell
    }
}

