//
//  ProofTypeCollectionViewCell.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class ProofTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var lineView : UIView!
    
    func setData(proofType:ProofType , selected:Bool){
        titleLabel.text = proofType.proofTypeTitle
        titleLabel.textColor = selected ? UIColor.init(codeString: "#00018E") : UIColor.init(codeString: "#222222")
        lineView.isHidden = !selected
    }
}
