//
//  ProofTableViewCell.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class ProofTableViewCell: UITableViewCell {
    
    @IBOutlet weak var proofTitleLabel : UILabel!
    @IBOutlet weak var proofLocationLabel : UILabel!


    func setData(proof:Proof){
        proofTitleLabel.text = proof.name ?? ""
        proofLocationLabel.text = proof.address ?? ""
    }
}
