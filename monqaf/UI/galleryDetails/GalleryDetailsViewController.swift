//
//  GalleryDetailsViewController.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class GalleryDetailsViewController: UIViewController {
    
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var collectionView : UICollectionView!

    var media = Offer()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setviewData()
    }
    
    func setDelegate(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    func setviewData(){
        titleLabel.text = media.name ?? ""
        var mDate = media.date ?? ""
        let indexStartOfText = mDate.index(mDate.startIndex, offsetBy: 10)
        mDate =  String(mDate[..<indexStartOfText])
        dateLabel.text = mDate.getDate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setDelegate()
        self.collectionView.reloadData()
    }
}

extension GalleryDetailsViewController : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return media.images?.count ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath)
        -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
            cell.setData(imageURL: media.images?[indexPath.row] ?? "")
            return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let viewWidth = self.view.bounds.width
        let widthPerItem = (viewWidth - 42) / 2.0
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.openGallery(startIndex: indexPath.row)
    }

    func openGallery(startIndex:Int)  {
      var images = [SKPhoto]()
        for image in self.media.images ?? [String](){
            let photo = SKPhoto.photoWithImageURL(image.getCloudNariImageUrl())
            photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
            images.append(photo)
        }
      let browser = SKPhotoBrowser(photos: images)
      SKPhotoBrowserOptions.displayCounterLabel = true   // counter label will be hidden
      SKPhotoBrowserOptions.displayBackAndForwardButton = false // back / forward button will be hidden
      SKPhotoBrowserOptions.displayAction = false // action button will be hidden
      SKPhotoBrowserOptions.displayHorizontalScrollIndicator = true // horizontal scroll bar will be hidden
      SKPhotoBrowserOptions.displayVerticalScrollIndicator = false
      browser.initializePageIndex(startIndex)
      self.present(browser, animated: true, completion: nil)
    }

}

