//
//  GalleryCollectionViewCell.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var gallerImageView : UIImageView!
    
    func setData(imageURL : String){
        gallerImageView.sd_setImage(with: URL.init(string: imageURL.getCloudNariImageUrl()), completed: nil)
    }
}
