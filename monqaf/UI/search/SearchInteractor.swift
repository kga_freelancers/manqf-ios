//
//  SearchInteractor.swift
//  monqaf
//
//  Created Amr Ahmed on 11/5/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class SearchInteractor : PresenterToIntetractorSearchProtocol {
    
    
    
    var presenter: InteractorToPresenterSearchProtocol?
    
    
    func loadSearchResult(text: String) {
        let dataSource = GeneralDataSource()
        dataSource.loadSearch(text: text){ (status, response) in
            switch status {
              case .sucess :
                self.presenter?.populateSearchResult(response: response as? BarcodeResponse ??  BarcodeResponse())
                  break
              case .error :
                self.presenter?.showError(messsage: response as? String ?? "")
                  break
              case .networkError:
                  self.presenter?.showError(messsage: response as? String ?? "")
                  break
              }
        }
    }
}

