//
//  ProductTableViewCell.swift
//  monqaf
//
//  Created by Amr Ahmed on 11/5/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import SDWebImage

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var price : UILabel!
    @IBOutlet weak var productImage : UIImageView!
    @IBOutlet weak var productcategoery : UILabel!


    
    func setData(product : Barcode)  {
        name.text = product.shortName
        price.text = "\(product.price!)"
        productcategoery.text = product.categoryName
        productImage.sd_setImage(with: URL.init(string: product.imageURL?.getCloudNariImageUrl() ?? ""), completed: nil)
    }

    

}
