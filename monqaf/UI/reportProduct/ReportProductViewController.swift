//
//  ReportProductViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import KVNProgress
class ReportProductViewController: ImagePickerViewController {

    @IBOutlet weak var nameField : UITextField!
    @IBOutlet weak var phoneField : UITextField!
    @IBOutlet weak var charityNameField : UITextField!
    @IBOutlet weak var messageField : UITextView!
    @IBOutlet weak var errorName : UILabel!
    @IBOutlet weak var errorPhone : UILabel!
    @IBOutlet weak var errorCharity : UILabel!
    @IBOutlet weak var errormessage: UILabel!
    @IBOutlet weak var productImageView: UIImageView!

    let model = ContactUsModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegates()
        addTargets()
        self.getImageResult()
    }
   
      
    func setDelegates() {
          nameField.delegate = self
          phoneField.delegate = self
          charityNameField.delegate = self
          messageField.delegate = self
      }
    
    func addTargets() {
            nameField.addTarget(self, action:#selector(ReportProductViewController.textFieldDidChange(_:)),
                                      for: UIControl.Event.editingChanged)
            phoneField.addTarget(self, action: #selector(ReportProductViewController.textFieldDidChange(_:)),
                                for: UIControl.Event.editingChanged)
           charityNameField.addTarget(self, action: #selector(ReportProductViewController.textFieldDidChange(_:)),
                                       for: UIControl.Event.editingChanged)
    }
    
    private func getImageResult(){
             self.completion = {(image,video) in
                DispatchQueue.main.async {
                    self.productImageView.image = image
                }
             }
        }
    
    func validate() -> Bool {
           var isValid = true
           if !(nameField.text?.count ?? 0 > 0) {
               isValid = false
               errorName.isHidden = false
           }
           if !(phoneField.text?.count ?? 0 > 0) {
               isValid = false
               errorPhone.isHidden = false
           }
           if !(charityNameField.text?.count ?? 0 > 0) {
               isValid = false
               errorCharity.isHidden = false
           }
           if !(messageField.text?.count ?? 0 > 0){
               isValid = false
               errormessage.isHidden = false
           }
           return isValid
       }
    
    
      @IBAction func sendAction(_sender :Any){
          if (!validate()){
              return
          }
          submitForm()
      }
    
    @IBAction func pickImageAction(_sender :Any){
        self.checkCamerAccess()
    }
    
    func submitForm() {
        
        model.phone = phoneField.text
        model.msg = messageField.text
        model.name = nameField.text
        model.cooperativeName = charityNameField.text
        KVNProgress.show()
        let dataSource = GeneralDataSource()
        dataSource.submitProductReport(contactUsModel : model){ (status, response) in
            KVNProgress.dismiss()
            switch status {
            case .sucess :
                self.showMessage("تم ارسال طلبك بنجاح ، شكرا علي ثقتكم بنا") {self.dismiss(animated: true, completion: nil)}
                break
            case .error :
                self.showMessage(response as! String)
                break
            case .networkError:
                self.showMessage(response as! String)
                break
            }
        }
    }
    
}

extension ReportProductViewController : UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == nameField && (nameField.text?.count)! > 0 {
            errorName.isHidden = true
        }
        
        if textField == phoneField && (phoneField.text?.count)! > 0 {
            errorPhone.isHidden = true
        }
        if textField == charityNameField && (charityNameField.text?.count)! > 0 {
                   errorCharity.isHidden = true
        }
      
    }
    
}

extension ReportProductViewController : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == messageField && (messageField.text?.count)! > 0 {
                errormessage.isHidden = true
        }
    }
}

