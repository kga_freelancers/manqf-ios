//
//  OfferPageCell.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import FSPagerView

class OfferPageCell: FSPagerViewCell {
    
    @IBOutlet weak var sliderImageView : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(imageURL : String) {
        sliderImageView.sd_setImage(with: URL.init(string: imageURL.getCloudNariImageUrl()), completed: nil)
    }

}
