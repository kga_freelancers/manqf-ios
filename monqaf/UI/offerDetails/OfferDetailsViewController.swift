//
//  OfferDetailsViewController.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import FSPagerView
import SKPhotoBrowser


class OfferDetailsViewController: UIViewController {
    
    @IBOutlet weak var offerDateLabel : UILabel!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var offerTitleLabel : UILabel!
    @IBOutlet weak var offerDescriptionLabel : UILabel!
    @IBOutlet weak var pageControl : FSPageControl!
    @IBOutlet weak var pagerController: FSPagerView!{
           didSet {
               self.pagerController.register(UINib(nibName: "OfferPageCell", bundle: nil), forCellWithReuseIdentifier: "OfferPageCell")
               self.pagerController.interitemSpacing = 0
           }
       }
    
    var offer = Offer()
    var headerTitle = ""
    var news = News()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = headerTitle
        self.setupPager()
        if offer.id != nil {
            self.setOfferData()
        }else{
            self.setNewsData()
        }
    }
    
    func openGallery(startIndex:Int)  {
      var images = [SKPhoto]()
        if offer.id != nil {
            for image in self.offer.images ?? [String](){
                let photo = SKPhoto.photoWithImageURL(image.getCloudNariImageUrl())
                photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                images.append(photo)
            }
        }else{
            for image in self.news.images ?? [String](){
                let photo = SKPhoto.photoWithImageURL(image.getCloudNariImageUrl())
                photo.shouldCachePhotoURLImage = true // you can use image cache by true(NSCache)
                images.append(photo)
            }
        }
      let browser = SKPhotoBrowser(photos: images)
      SKPhotoBrowserOptions.displayCounterLabel = true   // counter label will be hidden
      SKPhotoBrowserOptions.displayBackAndForwardButton = false // back / forward button will be hidden
      SKPhotoBrowserOptions.displayAction = true // action button will be hidden
      SKPhotoBrowserOptions.displayHorizontalScrollIndicator = true // horizontal scroll bar will be hidden
      SKPhotoBrowserOptions.displayVerticalScrollIndicator = false
      browser.initializePageIndex(startIndex)
      self.present(browser, animated: true, completion: nil)
    }
    
    private func setupPager(){
        pagerController.delegate = self
        pagerController.dataSource = self
        if offer.id != nil {
            pageControl.numberOfPages = self.offer.images?.count ?? 0
        }else{
            pageControl.numberOfPages = self.news.images?.count ?? 0
        }
        pageControl.setFillColor(UIColor.init(codeString: "#FFC138"), for: .selected)
        pageControl.setFillColor(UIColor.init(codeString: "#00018E"), for: .normal)
    }
    
    private func setOfferData(){
        var mDate = offer.date ?? ""
        let indexStartOfText = mDate.index(mDate.startIndex, offsetBy: 10)
        mDate =  String(mDate[..<indexStartOfText])
    
        var toDate = offer.toDate ?? ""
        if(!toDate.isEmpty && toDate.count >= 10){
            let indexStartOfToText = toDate.index(toDate.startIndex, offsetBy: 10)
            toDate =  String(toDate[..<indexStartOfToText])
        }

        if(!toDate.isEmpty && toDate.count >= 10){
            offerDateLabel.text = " من " + mDate.getDate() + " الي " + toDate.getDate()
        }
        else{
             offerDateLabel.text = mDate.getDate()
        }
        offerTitleLabel.text = offer.title
        offerDescriptionLabel.text = offer.offerDescription
    }
    
    private func setNewsData(){
          var mDate = news.date ?? ""
          let indexStartOfText = mDate.index(mDate.startIndex, offsetBy: 10)
          mDate =  String(mDate[..<indexStartOfText])
          offerDateLabel.text = mDate.getDate()
          offerTitleLabel.text = news.title
          offerDescriptionLabel.text = news.newsDescription
      }
    
    @IBAction func share (_ sender:Any){
         var myLink = ((offer.title ?? "") + ((":" + (offer.offerDescription ?? "")) + ("لمتابعة اخر الأخبار المتعلقة بالجمعيه حمل تطبيق جمعيه المنقف من خلال هذا الرابط")))
        myLink = myLink + "\n" + Constants.APP_Link
        Utils.share(vc: self, link: myLink)
        
    }
}

extension OfferDetailsViewController : FSPagerViewDataSource
{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        if offer.id != nil {
            return offer.images?.count ?? 0
        }else{
           return self.news.images?.count ?? 0
        }
        return 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "OfferPageCell", at: index) as!  OfferPageCell
        
        if offer.id != nil {
            cell.setData(imageURL: self.offer.images?[index] ?? "")
        }else{
            cell.setData(imageURL: self.news.images?[index] ?? "")
        }
        pageControl.currentPage = index
        return cell
    }
    

    
    @objc func SwipeLeftImage(){
        print("Swipe Left")
    }
    @objc func SwipeRightImage(){
        print("Swipe Right")
        
    }
}

extension OfferDetailsViewController : FSPagerViewDelegate
{
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int)
    {
        self.openGallery(startIndex: index)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        // update arrows and title
        pageControl.currentPage = targetIndex
        
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        pageControl.currentPage = index
     }
}
