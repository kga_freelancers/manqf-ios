//
//  BaseApi.swift
//  ADSC
//
//  Created by SherifShokry on 8/6/19.
//  Copyright © 2019 SherifShokry. All rights reserved.
//

import Foundation
import Alamofire

class BaseDataSource : Any {
    
    struct  Constants {
        static var PER_PAGE = 1
        static var BASE_URL = "http://cooperative.mangafcoop.com/api/"
        static var OFFERS = BASE_URL + "Offers/GetSlider?Take=8"
        static var OFFER_BY_ID = BASE_URL + "Offers/GetOfferById?Id="
        static var NEWS_BY_ID = BASE_URL + "News/GetNewsById?Id="
        static var BOARD_MEMBERS = BASE_URL + "Directors/GetDirectors"
        static var NEWS = BASE_URL + "News/GetNews"
        static var BRANCHES = BASE_URL + "BranchService/GetBranchService?BranchType="
        static var PROFITS = BASE_URL + "Shareholder/GetShareholder?CivilId="
        static var OFFERS_LIST = BASE_URL + "Offers/GetOffers"
        static var GALLERY_LIST = BASE_URL + "Gallery/GetGallery"
        static var BARCODE = BASE_URL + "item/GetByBarCode"
        static var PROOFS = BASE_URL + "Guideline/GetGuideline"
        static var QUESTIONS = BASE_URL + "Question/GetQuestion"
        static var SUBMIT_ANSWER = BASE_URL + "Answer/CreateAnswer"
        static var CONTACT_US = BASE_URL + "ContactUs/CreateContactUs"
        static var REPORT_PRODUCT = BASE_URL + "ItemReporting/CreateItemReporting"
        static var PRODUCT_SEARCH = BASE_URL + "item/SearchItem?Search="
        static var PRODUCT_LIST = BASE_URL + "item/GetItems"

    
        
        // user APIs
        static var WALKTHROUGH = BASE_URL + "walk-through"
        static var LOGIN = BASE_URL + "login"
        static var LOAD_COUNTRIES = BASE_URL + "get-countries"
        static var PROFILE = BASE_URL + "profile"
        static var USER_CHECK = BASE_URL + "check-phone-or-email"
        static var REGISTER = BASE_URL + "register"
        static var ADD_TOKEN = BASE_URL + "add-update-device"
        static var SOCIAL_REGISTER = BASE_URL + "register-social"
        static var CHECK_PHONE_AVAILABILITY = BASE_URL + "check-phone-or-email"
        static var RESET_PASSWORD = BASE_URL + "reset-password"
        static var SEND_SUGGESTION = BASE_URL + "send-suggestion"
        static var ADD_USER_INTERESTS = BASE_URL + "add-user-interests"
        static var POINTS_HISTORY = BASE_URL + "user-points"
        static var UPDATE_PROFILE = BASE_URL + "update-profile"
        static var CHANGE_PASSWORD = BASE_URL + "change-password"


        // challenge APIS
        static var WEEKLY_CHALLENGE = BASE_URL + "challenge-of-the-week"
        static var SUBMIT_PROOF = BASE_URL + "submit-proof"
        
        //Notifications
        static var NOTIFICATION = BASE_URL +  "notifications"
        static var DELETE_NOTIFICATION = BASE_URL + "delete-notification"
    }
    
    public enum ResponseStatus: String {
        case error = "error"
        case sucess = "success"
        case networkError = "networkError"
    }
    
    func BaseAPI(url:String , method: Alamofire.HTTPMethod,params:[String:Any]? , headers :[String:Any]? ,completion:@escaping([String:Any]?,Error?)->Void){
        Alamofire.request(url, method:method, parameters:params, encoding:JSONEncoding.default, headers:headers as? [String : String])
            .responseJSON { (response) in
                switch(response.result){
                case .success(_):
                    let json = response.result.value as? [String: Any]
                    completion(json,nil)
                    break
                case .failure(let error):
                    if let data = response.data {
                        print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                    }
                    print(error)
                    completion(nil,response.error)
                    break
                }
        }
    }
    
    func uploadProfile(name : String ,profile : UIImage? ,  completion:@escaping([String:Any]?,Error?)->Void) {
        var data : Data? = nil
        if profile != nil{
            data = profile!.jpegData(compressionQuality: 0.9)
        }
        
        let header : HTTPHeaders = getHeader() as! HTTPHeaders
        
        Alamofire.upload(multipartFormData: { (form) in
            form.append(name.data(using: .utf8)!, withName:  "name")
            if data != nil{
                form.append(data!, withName: "profile_picture", fileName: "file.png", mimeType: "image/png")
            }
        }, to: "" ,
           method:.post ,
           headers : header ,
           encodingCompletion: { result in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    switch(response.result){
                    case .success(_):
                        print(response.result.value ?? "")
                        let json = response.result.value as? [String: Any]
                        completion(json,nil)
                        break
                    case .failure(_):
                        completion(nil,response.error)
                        break
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                completion(nil,encodingError)
            }
        })
    }
    
    func getHeader ()->[String : Any]{
        var header = [
            "Content-Type" : "application/json" ,
            "Accept" : "application/json",
            "Accept-Language" : LocalizationSystem.sharedInstance.getLanguage()]
        
        return header
    }
    
    
}
