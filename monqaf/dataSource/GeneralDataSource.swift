//
//  GeneralDataSource.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class GeneralDataSource: BaseDataSource {
    
    
    
    func loadHome(completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.OFFERS
        

        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = SliderResponse.getSliderResponse(dict: json ?? [:])
            
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadBoardMembers(completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.BOARD_MEMBERS
        
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = BoardMemberResponse.getBoardMembersResponse(dict: json ?? [:])
                
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadNews(completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.NEWS + "?NewsType=1"
        
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = NewsResponse.getNewsResponse(dict: json ?? [:])
                
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadSocialNews(completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.NEWS + "?NewsType=2"
        
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = NewsResponse.getNewsResponse(dict: json ?? [:])
                
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadOffers(pageNumber:Int,completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.OFFERS_LIST +
            "?SkipCount=\(pageNumber * Constants.PER_PAGE)" +
        "&MaxResultCount=\(Constants.PER_PAGE)"
  
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = OffersResponse.getOffersResponse(dict: json ?? [:])
                
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadGallery(completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.GALLERY_LIST
        
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = OffersResponse.getOffersResponse(dict: json ?? [:])
                
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadBarcodeResult(barcode:String,completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.BARCODE + "?BarCode=" + barcode
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = BarcodeResponse.getBarcodeResponse(dict: json ?? [:])
                
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    
    func loadBranches(typ: Int,completion:@escaping(ResponseStatus,Any)->Void) {
        let url = BaseDataSource.Constants.BRANCHES + "\(typ)"
        
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = BranchesResponse.getBranchesResponse(dict: json ?? [:])
                
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadProfits(nationalId: String,boxNumber : String,completion:@escaping(ResponseStatus,Any)->Void) {
        
       let url = BaseDataSource.Constants.PROFITS + nationalId + "&BoxNo=" + boxNumber
        
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = ProfitsResponse.getProfitsResponse(dict: json ?? [:])
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func loadProofs(completion:@escaping(ResponseStatus,Any)->Void) {
           
          let url = BaseDataSource.Constants.PROOFS
           
           
           BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
               if json != nil{
                   let response = ProofResponse.getProofResponse(dict: json ?? [:])
                   if response.success ?? false  {
                       completion(.sucess,response)
                   }
                   else {
                       completion(.error,response.error ?? "")
                   }
               }else{
                   completion(.error,"Check your internet connection".localize())
               }
           }
       }
    
    func loadQuestions(completion:@escaping(ResponseStatus,Any)->Void) {
        
       let url = BaseDataSource.Constants.QUESTIONS
        
        
        BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = QuestionResponse.getQuestionResponse(dict: json ?? [:])
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func submitAnswer(answer:Answer,completion:@escaping(ResponseStatus,Any)->Void) {
        
       let url = BaseDataSource.Constants.SUBMIT_ANSWER
        
        var params = [String:Any]()
        params["QuestionId"] = answer.questionID
        params["UserName"] = answer.username
        params["Phone"] = answer.phone
        params["BoxNo"] = answer.boxNumber
        params["UserAnswer"] = answer.answer
        
        BaseAPI(url: url, method: .post, params: params, headers: getHeader()) { (json, error) in
            if json != nil{
                let response = BaseResponse.getBaseResponse(dict: json ?? [:])
                if response.success ?? false  {
                    completion(.sucess,response)
                }
                else {
                    completion(.error,response.error ?? "")
                }
            }else{
                completion(.error,"Check your internet connection".localize())
            }
        }
    }
    
    func submitContactUs(contactUsModel : ContactUsModel,completion:@escaping(ResponseStatus,Any)->Void) {
           let url = BaseDataSource.Constants.CONTACT_US

        let params = ["Name" : contactUsModel.name ?? "",
                      "Phone" : contactUsModel.phone ?? "",
                      "Email" : contactUsModel.email ?? "",
                      "Msg" : contactUsModel.msg ?? "",
                      "ContactUsType" : contactUsModel.contactUsType ?? 0] as [String : Any]


           BaseAPI(url: url, method: .post, params: params, headers: getHeader()) { (json, error) in
               if json != nil{
                   let response = BaseResponse.getBaseResponse(dict: json ?? [:])

                   if response.success ?? false  {
                       completion(.sucess,response)
                   }
                   else {
                       completion(.error,response.error ?? "")
                   }
               }else{
                   completion(.error,"Check your internet connection".localize())
               }
           }
       }


    func submitProductReport(contactUsModel : ContactUsModel,completion:@escaping(ResponseStatus,Any)->Void) {
              let url = BaseDataSource.Constants.REPORT_PRODUCT

           let params = ["Name" : contactUsModel.name ?? "",
                         "Phone" : contactUsModel.phone ?? "",
                         "CooperativeName" : contactUsModel.cooperativeName ?? "",
                         "Notes" : contactUsModel.msg ?? "",
                         "ImageUrl" : contactUsModel.imageUrl ?? ""] as [String : Any]


              BaseAPI(url: url, method: .post, params: params, headers: getHeader()) { (json, error) in
                  if json != nil{
                      let response = BaseResponse.getBaseResponse(dict: json ?? [:])

                      if response.success ?? false  {
                          completion(.sucess,response)
                      }
                      else {
                          completion(.error,response.error ?? "")
                      }
                  }else{
                      completion(.error,"Check your internet connection".localize())
                  }
              }
          }
    
    func loadSearch(text : String,completion:@escaping(ResponseStatus,Any)->Void) {
        var url = ""
        if text == "" {
            url = BaseDataSource.Constants.PRODUCT_LIST
        }else{
            url = BaseDataSource.Constants.PRODUCT_SEARCH + text
        }
        url = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                        
                BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
                    if json != nil{
                        let response = BarcodeResponse.getBarcodeResponse(dict: json ?? [:])
                        
                        if response.success ?? false  {
                            completion(.sucess,response)
                        }
                        else {
                            completion(.error,response.error ?? "")
                        }
                    }else{
                        completion(.error,"Check your internet connection".localize())
                    }
                }
            }
    
    
    func loadOfferById(id : Int,completion:@escaping(ResponseStatus,Any)->Void) {
          let url = BaseDataSource.Constants.OFFER_BY_ID + "\(id)"
        
          BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
              if json != nil{
                  let response = OfferResponse.getOfferResponse(dict: json ?? [:])
              
                  if response.success ?? false  {
                    completion(.sucess,response.offer)
                  }
                  else {
                      completion(.error,response.error ?? "")
                  }
              }else{
                  completion(.error,"Check your internet connection".localize())
              }
          }
      }
    
    func loadNewsById(id : Int,completion:@escaping(ResponseStatus,Any)->Void) {
            let url = BaseDataSource.Constants.NEWS_BY_ID + "\(id)"
          
            BaseAPI(url: url, method: .get, params: nil, headers: getHeader()) { (json, error) in
                if json != nil{
                    let response = NewsDetailsResponse.getNewsDetailsResponse(dict: json ?? [:])
                
                    if response.success ?? false  {
                      completion(.sucess,response.news)
                    }
                    else {
                        completion(.error,response.error ?? "")
                    }
                }else{
                    completion(.error,"Check your internet connection".localize())
                }
            }
        }



}
