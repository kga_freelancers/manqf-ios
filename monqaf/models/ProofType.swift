//
//  ProofType.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class ProofType {
    var proofTypeTitle = ""
    var proofList = [Proof]()
    
    
    init(_ proofTypeTitle: String,_ proofList: [Proof]) {
         self.proofTypeTitle = proofTypeTitle
         self.proofList = proofList
     }
    
    static func construct(proofList : [Proof]) -> [ProofType]{
        var mList = [ProofType]()
        var dailyList = [Proof]()
        var satList = [Proof]()
        var sunList = [Proof]()
        var monList = [Proof]()
        var tueList = [Proof]()
        var wedList = [Proof]()
        var thurList = [Proof]()
        var friList = [Proof]()
        
        proofList.forEach { (proof) in
            if(proof.allDays ?? false){
                dailyList.append(proof)
            }
            if((proof.days ?? "").contains("1")){
                satList.append(proof)
            }
            if((proof.days ?? "").contains("2")){
                sunList.append(proof)
            }
            if((proof.days ?? "").contains("3")){
                monList.append(proof)
            }
            if((proof.days ?? "").contains("4")){
                tueList.append(proof)
            }
            if((proof.days ?? "").contains("5")){
                wedList.append(proof)
            }
            if((proof.days ?? "").contains("6")){
                thurList.append(proof)
            }
            if((proof.days ?? "").contains("7")){
                friList.append(proof)
            }
        }
        
        mList.append(ProofType("الكل",proofList))
        mList.append(ProofType("يوميا",dailyList))
        mList.append(ProofType("السبت",satList))
        mList.append(ProofType("الأحد",sunList))
        mList.append(ProofType("الأثنين",monList))
        mList.append(ProofType("الثلاثاء",tueList))
        mList.append(ProofType("الأربعاء",wedList))
        mList.append(ProofType("الخميس",thurList))
        mList.append(ProofType("الجمعة",friList))
        
        mList.reverse()
        
        return mList
    }
}
