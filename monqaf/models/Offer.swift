//
//  Offer.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import ObjectMapper

class Offer : Mappable {
    
    var id : Int?
    var images : [String]?
    var title : String?
    var date : String?
    var toDate : String?
    var place : String?
    var name : String?
    var offerDescription : String?
    
    init() {
    }
    
    required init?(map: Map) {
        id = -1
        images = [String]()
        title = ""
        date = ""
        toDate = ""
        place = ""
        name = ""
        offerDescription = ""
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        images <- map["images"]
        place <- map["place"]
        title <- map["title"]
        name <- map["name"]
        offerDescription <- map["description"]
        date <- map["date"]
        toDate <- map["toDate"]
    }

}

class OffersResponse: BaseResponse {
    var offersList : OffersList?
    
    override init() {
        super.init()
    }
    required init?(map: Map) {
        super.init(map: map)
    }
     override func mapping(map: Map) {
        super.mapping(map: map)
        offersList <- map["result"]
    }
    
    static func getOffersResponse(dict : [String:Any]) -> OffersResponse {
        return Mapper<OffersResponse>().map(JSON: dict)!
    }
}

class OffersList: Mappable {
    
    var list = [Offer]()
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        list <- map["items"]
    }
    
    init() {
    }

}


