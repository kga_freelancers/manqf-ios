//
//  Question.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper

class Question : Mappable {
    
    var id : Int?
    var title : String?
    var questionDescription : String?
    var imageURL : String?
    
    init() {
    }
    
    required init?(map: Map) {
        id = -1
        title = ""
        questionDescription = ""
        imageURL = ""
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        questionDescription <- map["description"]
        imageURL <- map["imageUrl"]
    }

}

class QuestionResponse: BaseResponse {
    var questionList : QuestionList?
    
    override init() {
        super.init()
    }
    required init?(map: Map) {
        super.init(map: map)
    }
     override func mapping(map: Map) {
        super.mapping(map: map)
        questionList <- map["result"]
    }
    
    static func getQuestionResponse(dict : [String:Any]) -> QuestionResponse {
        return Mapper<QuestionResponse>().map(JSON: dict)!
    }
}

class QuestionList: Mappable {
    
    var list = [Question]()
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        list <- map["items"]
    }
    
    init() {
    }

}

