//
//  Branch.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper

class Branch: Mappable {
    
    
    var name :  String?
    var imageUrl  :String?
    var branchType : Int?
    var address : String?
    var phone : String?
    var notes : String?
    var lat : Double?
    var lng : Double?
    var id : Int?
    
    func mapping(map: Map) {
        name <- map["name"]
        imageUrl <- map["imageUrl"]
        branchType <- map["branchType"]
        address <- map["address"]
        phone <- map["phone"]
        notes <- map["notes"]
        lat <- map["lat"]
        lng <- map["lng"]
        id <- map["id"]
    }
    
    required init?(map: Map) {
        
    }
    init() {
        
    }
}
