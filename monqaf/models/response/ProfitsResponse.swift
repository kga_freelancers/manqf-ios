//
//  ProfitsResponse.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfitsResponse: BaseResponse {

    var profitResult = ProfitResult()
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        profitResult <- map["result"]
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override init() {
        super.init()
    }
    
    static  func getProfitsResponse(dict : [String:Any]) ->  ProfitsResponse{
        return Mapper<ProfitsResponse>().map(JSON: dict)!
    }
}

class ProfitResult: Mappable {

    var profit = Profit()
    
    func mapping(map: Map) {
        profit <- map["shareholderDto"]
    }
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
}

class Profit: Mappable {
    

    var boxNo : String?
    var name : String?
    var civilId : String?
    var purchaseAmount : Int?
    var purchasePercentage : Int?
    var purchaseProfit : Int?
    var shareAmount : Int?
    var sharePercentage : Int?
    var shareProfit : Int?
    var totalProfit : Int?
    var id  = 0
    
    
    init() {
        
    }
    
    func mapping(map: Map) {
        boxNo <- map["boxNo"]
        name <- map["name"]
        civilId <- map["civilId"]
        purchaseAmount <- map["purchaseAmount"]
        purchasePercentage <- map["purchasePercentage"]
        purchaseProfit <- map["purchaseProfit"]
        shareAmount <- map["shareAmount"]
        sharePercentage <- map["sharePercentage"]
        shareProfit <- map["shareProfit"]
        totalProfit <- map["totalProfit"]
        id <- map["id"]
    }
    
    required init?(map: Map) {
        
    }
    
}

