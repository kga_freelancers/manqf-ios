//
//  BaseResponse.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseResponse : Mappable{
    
    var success : Bool?
    var error : String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
       
    }
    
    func mapping(map: Map) {
        success <- map["success"]
        error <- map["error"]
    }
    
   static func getBaseResponse(dict : [String:Any]) ->  BaseResponse{
        
        return Mapper<BaseResponse>().map(JSON: dict)!
    }

}
