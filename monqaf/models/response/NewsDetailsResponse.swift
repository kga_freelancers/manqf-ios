//
//  NewsDetailsResponse.swift
//  monqaf
//
//  Created by Amr Ahmed on 12/8/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsDetailsResponse:BaseResponse {
    
    var news = News()
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        news <- map["result"]
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    static func getNewsDetailsResponse(dict : [String:Any]) ->  NewsDetailsResponse{
        return Mapper<NewsDetailsResponse>().map(JSON: dict)!
    }
    
}
