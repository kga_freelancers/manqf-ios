//
//  BoardMemberResponse.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper


class BoardMemberResponse: BaseResponse {
    
    var boardMemberData : BoardMembersData?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        boardMemberData <- map["result"]
    }
    
    static  func getBoardMembersResponse(dict : [String:Any]) ->  BoardMemberResponse{
        return Mapper<BoardMemberResponse>().map(JSON: dict)!
    }
}

class BoardMembersData: Mappable {
   
    var memberList : [BoardMember]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        memberList <- map["items"]
    }
    
    
   
    
    
    
}

class BoardMember: Mappable {
    
    var name : String?
    var imageUrl : String?
    var position : String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        imageUrl <- map["imageUrl"]
        position <- map["position"]
    }
    
    
}
