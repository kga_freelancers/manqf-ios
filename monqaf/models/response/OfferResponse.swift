//
//  OfferResponse.swift
//  monqaf
//
//  Created by Amr Ahmed on 12/8/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper

class OfferResponse: BaseResponse{
    
    var offer = Offer()
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        offer <- map["result"]
    }
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    static func getOfferResponse(dict : [String:Any]) ->  OfferResponse{
        return Mapper<OfferResponse>().map(JSON: dict)!
    }

}
