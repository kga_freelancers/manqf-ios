//
//  BarnchesResponse.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper


class BranchesResponse: BaseResponse {
    
    var result = Result()

    
    override func mapping(map: Map) {
        super.mapping(map: map)
        result <- map["result"]
    }
    
    static  func getBranchesResponse(dict : [String:Any]) ->  BranchesResponse{
        return Mapper<BranchesResponse>().map(JSON: dict)!
    }
}


class Result: Mappable {
    var brabchList = [Branch]()
    
    func mapping(map: Map) {
        brabchList <- map["branchServiceDtos"]
    }
 
    required init?(map: Map) {
    }
    
    init() {
        
    }
}
