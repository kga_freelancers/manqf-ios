//
//  Slider.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper



class SliderResponse: BaseResponse{
    
    var sliderData : SliderData?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        sliderData <- map["result"]
    }
    
    override init() {
        super.init()
    }
    
  static  func getSliderResponse(dict : [String:Any]) ->  SliderResponse{
        
        return Mapper<SliderResponse>().map(JSON: dict)!
    }
    
}

class SliderData: Mappable {
    
    var sliderList : [Slider]?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        sliderList <- map["sliderDto"]
    }
    
    
}

class Slider: Mappable {
   
    var id : Int?
    var title : String?
    var date : String?
    var imageUrl : String?
    var sliderType : Int?
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        date <- map["date"]
        imageUrl <- map["imageUrl"]
        sliderType <- map["sliderType"]
    }
    

    
    
}
