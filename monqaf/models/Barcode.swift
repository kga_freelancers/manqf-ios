//
//  Barcode.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/19/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit
import ObjectMapper


class Barcode : Mappable {
    
    var id : Int?
    var imageURL : String?
    var code : String?
    var shortName : String?
    var shortNameEn : String?
    var unitId : Int?
    var unitName : String?
    var quantity : String?
    var barcode : String?
    var price : Double?
    var categoryID : Int?
    var categoryName : String?
    var subCategoryID : Int?
    var subCategoryName : String?

    
    
    init() {
    }
    
    required init?(map: Map) {
        id = -1
        imageURL = ""
        code = ""
        shortName = ""
        shortNameEn = ""
        unitId = -1
        unitName = ""
        quantity = ""
        barcode = ""
        price = -1
        categoryID = -1
        categoryName = ""
        subCategoryID = -1
        subCategoryName = ""
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        imageURL <- map["imageUrl"]
        code <- map["code"]
        shortName <- map["shortName"]
        shortNameEn <- map["shortNameEn"]
        unitId <- map["unitId"]
        unitName <- map["unitName"]
        quantity <- map["quantity"]
        barcode <- map["barCode"]
        price <- map["price"]
        categoryID <- map["categoryId"]
        categoryName <- map["categoryName"]
        subCategoryID <- map["subCategoryId"]
        subCategoryName <- map["subCategoryName"]
    }

}

class BarcodeResponse: BaseResponse {
    var barcodeData : BarcodeData?
    
    override init() {
        super.init()
    }
    required init?(map: Map) {
        super.init(map: map)
    }
     override func mapping(map: Map) {
        super.mapping(map: map)
        barcodeData <- map["result"]
    }
    
    static func getBarcodeResponse(dict : [String:Any]) ->  BarcodeResponse {
        return Mapper<BarcodeResponse>().map(JSON: dict)!
    }
}

class BarcodeData: Mappable {
    
    var barCode = Barcode()
    var productsList = [Barcode]()
    var searchList = [Barcode]()

    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        barCode <- map["itemDto"]
        productsList <- map["itemDtos"]
        searchList <- map["items"]
    }
    
    init() {
    }

}


