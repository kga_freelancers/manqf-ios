//
//  Proof.swift
//  monqaf
//
//  Created by Karim abdelhameed mohamed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import ObjectMapper

class Proof : Mappable {
    
    var id : Int?
    var days : String?
    var name : String?
    var address : String?
    var phone : String?
    var notes : String?
    var allDays : Bool?
    var lat : Double?
    var lng : Double?
    
    init() {
    }
    
    required init?(map: Map) {
        id = -1
        days = ""
        name = ""
        address = ""
        phone = ""
        notes = ""
        allDays = false
        lat = 0.0
        lng = 0.0
    }
    
     func mapping(map: Map) {
        id <- map["id"]
        days <- map["days"]
        name <- map["name"]
        address <- map["address"]
        phone <- map["phone"]
        notes <- map["notes"]
        allDays <- map["allDays"]
        lat <- map["lat"]
        lng <- map["lng"]
    }

}

class ProofResponse: BaseResponse {
    var proofList : ProofList?
    
    override init() {
        super.init()
    }
    required init?(map: Map) {
        super.init(map: map)
    }
     override func mapping(map: Map) {
        super.mapping(map: map)
        proofList <- map["result"]
    }
    
    static func getProofResponse(dict : [String:Any]) -> ProofResponse {
        return Mapper<ProofResponse>().map(JSON: dict)!
    }
}

class ProofList: Mappable {
    
    var list = [Proof]()
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        list <- map["items"]
    }
    
    init() {
    }

}

