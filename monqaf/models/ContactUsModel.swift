//
//  ContactUsModel.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/26/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class ContactUsModel {
   
    var name : String?
    var phone : String?
    var email : String?
    var msg : String?
    var contactUsType : Int?
    var cooperativeName : String?
    var imageUrl : String?

}
