//
//  UIFont.swift
//  Fresh
//
//  Created by Karim abdelhameed mohamed on 1/29/18.
//  Copyright © 2018 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

struct AppFontName {
    
    static let regular = "NEOSANS"
    static let bold = "NEOSANSARABIC-MEDIUM"
    static let italic = "NEOSANSARABIC"
    static let semiBold = "NEOSANSARABIC"
    static let light = "NEOSANSARABIC"
    static let heavy = "NEOSANSARABIC"
    
   
    static let regular_en = "NEOSANS"
    static let bold_en = "NEOSANS-BOLD"
    static let italic_en = "NEOSANS"
    static let semiBold_en = "NEOSANS-MEDIUM"
    static let light_en = "NEOSANS-LIGHT"
    static let heavy_en = "NEOSANS-MEDIUM"

}

extension UIFont {
    
 
    @objc class func mySystemFont(ofSize size: CGFloat) -> UIFont {
   return UIFont(name: AppFontName.regular_en, size: size)!
    }
    
    @objc class func mySystemFontForEN(ofSize size: CGFloat) -> UIFont {
         print(size * Constants.fontScale)
        return UIFont(name: AppFontName.regular_en, size: size)!
    }
    
    @objc class func myBoldSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.bold_en, size: size)!
    }
    
    @objc class func myBoldSystemFontForEN(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.bold_en, size: size)!
    }
    
    @objc class func myItalicSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.italic_en, size: size)!
    }
    
    @objc class func myItalicSystemFontForEN(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.italic_en, size: size )!
    }
    
    @objc class func myHeavySystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.heavy_en,size:size)!
    }
    
    @objc class func myHeavySystemFontForEN(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.heavy_en, size: size)!
    }
   
    
    @objc class func myLightSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.light_en, size: size)!
    }
    
    @objc class func myLightSystemFontForEN(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.light_en, size: size )!
    }
    
    
    @objc class func mySemiBoldSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.semiBold_en, size: size)!
    }
    
    @objc class func mySemiBoldSystemFontForEN(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: AppFontName.semiBold_en, size: size)!
    }
    

    
    @objc convenience init(myCoder aDecoder: NSCoder) {
        if let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor {
            let mFontAttribute = UIFontDescriptor.AttributeName(rawValue: "NSFontNameAttribute")
            
            if let fontAttribute = fontDescriptor.fontAttributes[mFontAttribute] as? String {
                
                var fontName = ""
                switch fontAttribute {
                case AppFontName.regular_en :
                    if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                            fontName = AppFontName.regular
                    }else{
                         fontName = AppFontName.regular_en
                    }
                
                case  AppFontName.bold_en:
                    if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                    fontName = AppFontName.bold
                    }else{
                    fontName = AppFontName.bold_en
                    }
                
                case  AppFontName.italic_en:
                    if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                         fontName = AppFontName.italic
                    }else{
                        fontName = AppFontName.italic_en
                    }
                   
                case  AppFontName.semiBold_en:
                    if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                      fontName = AppFontName.semiBold
                    }else{
                      fontName = AppFontName.semiBold_en
                    }
                    
                case  AppFontName.light_en:
                    if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                        fontName = AppFontName.light
                    }else{
                        fontName = AppFontName.light_en
                    }
                  
                case AppFontName.heavy_en :
                    if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                          fontName = AppFontName.heavy
                    }else{
                         fontName = AppFontName.heavy_en
                    }
                 
                default:
                    if LocalizationSystem.sharedInstance.getLanguage() == "ar"{
                       fontName = AppFontName.regular
                    }else{
                       fontName = AppFontName.regular_en
                    }
                    
                }
                self.init(name: fontName, size: fontDescriptor.pointSize * Constants.fontScale)!
            }
            else {
                
                self.init(myCoder: aDecoder)
            }
        }
        else {
            
            self.init(myCoder: aDecoder)
        }
    }
    
    
    
    class func overrideInitialize(scale : Float) {
        
        if self == UIFont.self {
            let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:)))
            let mySystemFontMethod = class_getClassMethod(self, #selector(mySystemFont(ofSize:)))
            method_exchangeImplementations(systemFontMethod!, mySystemFontMethod!)
            
            let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:)))
            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldSystemFont(ofSize:)))
            method_exchangeImplementations(boldSystemFontMethod!, myBoldSystemFontMethod!)
            
            let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:)))
            let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myItalicSystemFont(ofSize:)))
            method_exchangeImplementations(italicSystemFontMethod!, myItalicSystemFontMethod!)
            
            let lightSystemFontMethod = class_getClassMethod(self, #selector(myLightSystemFont(ofSize:)))
            let myLightSystemFontMethod = class_getClassMethod(self, #selector(myLightSystemFont(ofSize:)))
            method_exchangeImplementations(lightSystemFontMethod!, myLightSystemFontMethod!)
            
            let semiBoldSystemFontMethod = class_getClassMethod(self, #selector(mySemiBoldSystemFont(ofSize:)))
            let mySemiBoldSystemFontMethod = class_getClassMethod(self, #selector(mySemiBoldSystemFont(ofSize:)))
            method_exchangeImplementations(semiBoldSystemFontMethod!, mySemiBoldSystemFontMethod!)
            
            let heavySystemFontMethod = class_getClassMethod(self, #selector(myHeavySystemFont(ofSize:)))
            let myHeavySystemFontMethod = class_getClassMethod(self, #selector(myHeavySystemFont(ofSize:)))
            method_exchangeImplementations(heavySystemFontMethod!, myHeavySystemFontMethod!)
            
            let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))) // Trick to get over the lack of UIFont.init(coder:))
            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:)))
            method_exchangeImplementations(initCoderMethod!, myInitCoderMethod!)
        }
    }
    
    
    class func overrideInitialize() {
        if self == UIFont.self {
            let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:)))
            let mySystemFontMethod = class_getClassMethod(self, #selector(mySystemFont(ofSize:)))
            method_exchangeImplementations(systemFontMethod!, mySystemFontMethod!)
            
            let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:)))
            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldSystemFont(ofSize:)))
            method_exchangeImplementations(boldSystemFontMethod!, myBoldSystemFontMethod!)
            
            let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:)))
            let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myItalicSystemFont(ofSize:)))
            method_exchangeImplementations(italicSystemFontMethod!, myItalicSystemFontMethod!)
            
            let lightSystemFontMethod = class_getClassMethod(self, #selector(myLightSystemFont(ofSize:)))
            let myLightSystemFontMethod = class_getClassMethod(self, #selector(myLightSystemFont(ofSize:)))
            method_exchangeImplementations(lightSystemFontMethod!, myLightSystemFontMethod!)
            
            let semiBoldSystemFontMethod = class_getClassMethod(self, #selector(mySemiBoldSystemFont(ofSize:)))
            let mySemiBoldSystemFontMethod = class_getClassMethod(self, #selector(mySemiBoldSystemFont(ofSize:)))
            method_exchangeImplementations(semiBoldSystemFontMethod!, mySemiBoldSystemFontMethod!)
            
            let heavySystemFontMethod = class_getClassMethod(self, #selector(myHeavySystemFont(ofSize:)))
            let myHeavySystemFontMethod = class_getClassMethod(self, #selector(myHeavySystemFont(ofSize:)))
            method_exchangeImplementations(heavySystemFontMethod!, myHeavySystemFontMethod!)
            
            let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))) // Trick to get over the lack of UIFont.init(coder:))
            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:)))
            method_exchangeImplementations(initCoderMethod!, myInitCoderMethod!)
        }
    }
    
    class func overrideInitializeForEN() {
        if self == UIFont.self {
            let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:)))
            let mySystemFontMethod = class_getClassMethod(self, #selector(mySystemFontForEN(ofSize:)))
            method_exchangeImplementations(systemFontMethod!, mySystemFontMethod!)
            
            let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:)))
            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldSystemFontForEN(ofSize:)))
            method_exchangeImplementations(boldSystemFontMethod!, myBoldSystemFontMethod!)
            
            let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:)))
            let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myItalicSystemFontForEN(ofSize:)))
            method_exchangeImplementations(italicSystemFontMethod!, myItalicSystemFontMethod!)
            
            let lightSystemFontMethod = class_getClassMethod(self, #selector(myLightSystemFont(ofSize:)))
            let myLightSystemFontMethod = class_getClassMethod(self, #selector(myLightSystemFontForEN(ofSize:)))
            method_exchangeImplementations(lightSystemFontMethod!, myLightSystemFontMethod!)
            
            let semiBoldSystemFontMethod = class_getClassMethod(self, #selector(mySemiBoldSystemFont(ofSize:)))
            let mySemiBoldSystemFontMethod = class_getClassMethod(self, #selector(mySemiBoldSystemFontForEN(ofSize:)))
            method_exchangeImplementations(semiBoldSystemFontMethod!, mySemiBoldSystemFontMethod!)
            
            let heavySystemFontMethod = class_getClassMethod(self, #selector(myHeavySystemFont(ofSize:)))
            let myHeavySystemFontMethod = class_getClassMethod(self, #selector(myHeavySystemFontForEN(ofSize:)))
            method_exchangeImplementations(heavySystemFontMethod!, myHeavySystemFontMethod!)
            
            let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))) // Trick to get over the lack of UIFont.init(coder:))
            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:)))
            method_exchangeImplementations(initCoderMethod!, myInitCoderMethod!)
        }
    }
    
    
    
    
    
}
