//
//  UIView.swift
//  AHA! Journal
//
//  Created by George Naiem on 9/3/16.
//  Copyright © 2016 ahealthadventure. All rights reserved.
//

import UIKit

extension UIView {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        if( UserDefaults.standard.string(forKey: Constants.APPT_THEME) == Constants.AppTheme.DARK){
            applyTheme()
        }
    }

    public func applyTheme() {
        if(self is AccentButton){
            let proxyAccentButton = (self as! AccentButton)
            proxyAccentButton.backgroundColor = Theme.accentButtonBackgroundColor
        }
            
        else if(self is WhiteButton){
            let proxyWhiteButton = (self as! WhiteButton)
            proxyWhiteButton.setTitleColor(Theme.accentButtonBackgroundColor, for: .normal)
            proxyWhiteButton.backgroundColor = Theme.whiteBgViewColor
        }
            
        else if(self is UIButton){
            let proxyButton = (self as! UIButton)
            proxyButton.setTitleColor(Theme.buttonTextColor, for: .normal)
            proxyButton.backgroundColor = Theme.buttonBackgroundColor
        }
        
        else if(self is BlackLabel){
            let proxyBlackLabel = (self as! BlackLabel)
            proxyBlackLabel.textColor = Theme.blackLabelColor
        }
            
        else if(self is SubTitleLabel){
            let proxySubTitleLabel = (self as! SubTitleLabel)
            proxySubTitleLabel.textColor = Theme.subTitleLabelColor
            
        }

        else if(self is UILabel){
            let proxyLabel = (self as! UILabel)
            proxyLabel.textColor = Theme.labelTextColor
            proxyLabel.backgroundColor = Theme.labelBgColor
        }
       
        else if(self is WhiteImage){
            let proxyWhiteImage = (self as! WhiteImage)
            proxyWhiteImage.image = proxyWhiteImage.image?.withRenderingMode(.alwaysTemplate)
            proxyWhiteImage.tintColor = Theme.whiteBgViewColor
        }
            
        else if(self is UIImageView){
            let proxyImage = (self as! UIImageView)
            proxyImage.backgroundColor = Theme.ImageBackgroundColor
        }

        else if (self is CustomView){
            let proxyCardView = (self as! CustomView)
            proxyCardView.backgroundColor = Theme.cardBgColor
        }

        
        else if(self is ClearView){
            let proxyClearView = (self as! ClearView)
            proxyClearView.backgroundColor = Theme.clearBgViewColor
        }
        else if(self is CustomTable){
            let proxyCustomTableView = (self as! CustomTable)
            proxyCustomTableView.backgroundColor = Theme.whiteBgViewColor
        }

        else if(self is WhiteView){
            let proxyWhiteView = (self as! WhiteView)
            proxyWhiteView.backgroundColor = Theme.whiteBgViewColor

        }

//        else if(self is ADSCInputFeild){
//            let proxyCustomTextField = (self as! ADSCInputFeild)
//            proxyCustomTextField.field.textColor = Theme.textFieldColor
//            proxyCustomTextField.field.placeHolderColor = Theme.textFieldColor
//            proxyCustomTextField.field.keyboardAppearance = Theme.keyboardAppearence ?? UIKeyboardAppearance.dark
//        }
        
        else if(self is UITextField){
            let proxyTextField = (self as! UITextField)
            proxyTextField.textColor = Theme.textFieldColor
            proxyTextField.placeHolderColor = Theme.textFieldColor
            proxyTextField.keyboardAppearance = Theme.keyboardAppearence ?? UIKeyboardAppearance.dark
        }
            
        else if (self is UITabBar){
            let proxyTabBar = (self as! UITabBar)
            proxyTabBar.barTintColor = Theme.backgroundColor
            proxyTabBar.isTranslucent = false
            proxyTabBar.backgroundImage = nil
        }
        
        else if(self is WhiteTextView){
            let proxyTextView = (self as! WhiteTextView)
            proxyTextView.textColor = Theme.whiteBgViewColor
            proxyTextView.backgroundColor = .clear
        }
        
        else{
                let proxyView = self
                proxyView.backgroundColor = Theme.backgroundColor
        }
    }
       @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var drobShadow: Bool {
        get{
            return false
        }
        set {
            self.dropShadow(apply: newValue)
            
        }
    }
   
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    
    func startRotating() {
        self.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations:
            { () -> Void in
                self.transform = self.transform.rotated(by: CGFloat(Double.pi/2))
        }) { (finished) -> Void in
            if self.tag == 10{
                self.startRotating()
            }else{
                self.isHidden = true
            }
        }
        tag = 10
    }

    
    
    func stopRotating() {
        
        tag = 0
        
    }
    
    func dropShadow(apply : Bool) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = apply ? UIColor.black.cgColor : UIColor.clear.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: -1, height: 0.5)
        self.layer.shadowRadius = 0.5
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func appIsArabic()->Bool{
        let language = Bundle.main.preferredLocalizations[0] as NSString
        if language.contains("ar"){
            return true
        }
        else{
            return false
        }
    }
    
}
