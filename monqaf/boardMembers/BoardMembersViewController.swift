//
//  BoardMembersViewController.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class BoardMembersViewController: UIViewController {

    var presenter : ViewToPresenterBoardMemberProtocol?
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var indicator : UIActivityIndicatorView!

    var boardList = [BoardMember]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelagates()
        presenter?.loadBoardMembers()
        indicator.isHidden = false
        indicator.startAnimating()
        
    }

    func setDelagates() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
}

extension BoardMembersViewController : PresenterToViewBoardMemberProtocol {
    func showHomeData(data: BoardMemberResponse) {
        self.boardList = data.boardMemberData?.memberList ?? [BoardMember]()
        indicator.isHidden = true
        self.tableView.reloadData()
        
    }
    
    func showError(error: String) {
        self.showMessage(error)
    }
}

extension BoardMembersViewController: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.boardList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BoardMemberTableCell") as! BoardMemberTableCell
        cell.setData(boardMember: boardList[indexPath.row])
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
        
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }

}
