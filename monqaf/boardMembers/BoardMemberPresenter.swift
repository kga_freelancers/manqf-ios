//
//  BoardMemberPresenter.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class BaordMemberPresenter : ViewToPresenterBoardMemberProtocol {
    

    var view: PresenterToViewBoardMemberProtocol?
    var interactor: PresenterToIntetractorBoardMemberProtocol?
    var router: PresenterToRouterBoardMemberProtocol?
    
    func loadBoardMembers() {
        interactor?.loadBoardMembers()
    }

}
extension BaordMemberPresenter : InteractorToPresenterBoardMemberProtocol{
    
    func showData(data: BoardMemberResponse) {
        view?.showHomeData(data: data)
    }
    
    func showError(error: String) {
        view?.showError(error: error)
    }
}

