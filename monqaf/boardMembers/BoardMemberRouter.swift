//
//  BoardMemberRouter.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

class BoardMemberRouter: PresenterToRouterBoardMemberProtocol {
    
    static func createModule() -> UIViewController {
        
        let view = BoardMembersViewController.instantiateFromStoryBoard(appStoryBoard: .Main)
        let presenter : ViewToPresenterBoardMemberProtocol & InteractorToPresenterBoardMemberProtocol = BaordMemberPresenter()
        
        let interactor : PresenterToIntetractorBoardMemberProtocol = BoardMemberInteractor()
        let router : PresenterToRouterBoardMemberProtocol = BoardMemberRouter()
        
        view.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = router
        interactor.presenter = presenter
        return view
    }
}
