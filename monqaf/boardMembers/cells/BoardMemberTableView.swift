//
//  BoardMemberTableView.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import UIKit

class BoardMemberTableCell: UITableViewCell {
    
    @IBOutlet weak var memberImage : UIImageView!
    @IBOutlet weak var memberName : UILabel!
    @IBOutlet weak var memberPosition : UILabel!


    func setData(boardMember : BoardMember)  {
        memberImage.sd_setImage(with: URL.init(string: boardMember.imageUrl?.getCloudNariImageUrl() ?? ""), completed: nil)
        memberName.text = boardMember.name
        memberPosition.text = boardMember.position
    }

}
