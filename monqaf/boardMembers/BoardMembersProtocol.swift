//
//  BoardMembersProtocol.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterBoardMemberProtocol: class {
    
    var view : PresenterToViewBoardMemberProtocol? {get set}
    var interactor : PresenterToIntetractorBoardMemberProtocol? {get set}
    var router : PresenterToRouterBoardMemberProtocol? {get set}
    func loadBoardMembers()
}

protocol PresenterToViewBoardMemberProtocol: class {
    func showHomeData(data : BoardMemberResponse)
    func showError(error : String)
}

protocol PresenterToIntetractorBoardMemberProtocol: class {
    var presenter: InteractorToPresenterBoardMemberProtocol? { get set }
    
    func loadBoardMembers()
    
}

protocol PresenterToRouterBoardMemberProtocol: class  {
    static func createModule() -> UIViewController
    
}

protocol InteractorToPresenterBoardMemberProtocol: class {
    func showData(data : BoardMemberResponse)
    func showError(error : String)
}
