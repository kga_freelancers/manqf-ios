//
//  BoardMemberInteractor.swift
//  monqaf
//
//  Created by Amr Ahmed on 10/12/19.
//  Copyright © 2019 bluecrunch. All rights reserved.
//

import Foundation

class BoardMemberInteractor : PresenterToIntetractorBoardMemberProtocol {
    var presenter: InteractorToPresenterBoardMemberProtocol?
    
    
    func loadBoardMembers() {
        let dataSource = GeneralDataSource()
        dataSource.loadBoardMembers { (status, response) in
            switch status {
            case .sucess :
                self.presenter?.showData(data: response as? BoardMemberResponse
                    ?? BoardMemberResponse())
                break
            case .error :
                self.presenter?.showError(error: response as? String ?? "")
                break
            case .networkError:
                self.presenter?.showError(error: response as? String ?? "")
                break
            }
        }
    }
}
